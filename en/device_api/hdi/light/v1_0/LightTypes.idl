/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Light
 * @{
 *
 * @brief Provides common APIs for the light service to access the light driver.
 *
 * 
 * After obtaining the light driver object or proxy, the service can call the APIs to obtain light information,
 * turn on or off lights, and set the light blinking mode based on the light type.
 * @since 3.1
 * @version 1.0
 */

/**
 * @file LightTypes.idl
 *
 * @brief Defines light data structures, including the light ID, basic light information, lighting mode, blinking parameters, color model, and lighting effect.
 *
 * @since 3.1
 * @version 1.0
 */

/**
 * @brief Defines the path for the package of the light module APIs.
 *
 * @since 3.1
 * @version 1.0
 */
package ohos.hdi.light.v1_0;

/**
 * @brief Enumerates the light types.
 *
 * @since 3.1
 * @version 1.0
 */
enum HdfLightId {
    HDF_LIGHT_ID_BATTERY = 1,           /**< Power light. */
    HDF_LIGHT_ID_NOTIFICATIONS = 2, /**< Notification light. */
    HDF_LIGHT_ID_ATTENTION = 3,         /**< Alarm light. */
    HDF_LIGHT_ID_BUTT = 4,              /**< Invalid type. */
};

/**
 * @brief Defines basic light information, including the light type, logical light name, number of physical lights in the logical controller, and custom information.
 *
 *
 *
 * @since 3.1
 * @version 1.0
 */
struct HdfLightInfo {
    String lightName;     /**< Logical light name. */
    int lightId;     /**< Light type. For details, see {@link HdfLightId}. */
    int lightNumber;    /**< Number of physical lights in the logical controller. */
    int reserved; /**< Custom information. */
};

/**
 * @brief Enumerates the lighting modes.
 *
 * @since 3.1
 * @version 1.0
 */
enum HdfLightFlashMode {
    HDF_LIGHT_FLASH_NONE = 0,   /**< Steady on. */
    HDF_LIGHT_FLASH_TIMED = 1,  /**< Blinking. */
    HDF_LIGHT_FLASH_GRADIENT = 2,   /**< Gradient. */
    HDF_LIGHT_FLASH_BUTT = 3, /**< Invalid mode. */
};

/**
 * @brief Defines the blinking parameters, including the blinking mode and on and off time of the light during the blinking period.
 *
 * 
 *
 * @since 3.1
 * @version 1.0
 */
struct HdfLightFlashEffect {
   int flashMode; /**< Blinking mode. For details, see {@link LightFlashMode}. */
   int onTime;        /**< Duration (in ms) for which the light remains on during the blinking period. */ */
   int offTime;       /**< Duration (in ms) for which the light remains off during the blinking period. */
};

/**
 * @brief Defines the red green blue (RGB) color model.
 *
 * @since 3.2
 * @version 1.0
 */
struct RGBColor {
    int brightness;    /**< Brightness value, which ranges from 0 to 255. */
    int r;    /**< Red value, which ranges from 0 to 255. */
    int g;    /**< Green value, which ranges from 0 to 255. */
    int b;    /**< Blue value, which ranges from 0 to 255. */
};

/**
 * @brief Defines the white red green blue (WRGB) color model.
 *
 * @since 3.2
 * @version 1.0
 */
struct WRGBColor {
    int w;    /**< White value, which ranges from 0 to 255. */
    int r;    /**< Red value, which ranges from 0 to 255. */
    int g;    /**< Green value, which ranges from 0 to 255. */
    int b;    /**< Blue value, which ranges from 0 to 255. */
};

/**
 * @brief Defines the light color model.
 *
 * 
 *
 * @since 3.2
 * @version 1.0
 */
union ColorValue
{
    struct WRGBColor wrgbColor;    /**< WRGB model. For details, see {@link WRGBColor}. */
    struct RGBColor rgbColor;    /**< RGB model. For details, see {@link RGBColor}. */
};

/**
 * @brief Defines the lighting parameters, including the light color model.
 *
 * 
 *
 * @since 3.2
 * @version 1.0
 */
struct HdfLightColor {
    union ColorValue colorValue;    /**< Light color model. For details, see {@link ColorValue}. */
};

/**
 * @brief Defines the lighting effect parameters, including the brightness and blinking parameters.
 *
 * 
 *
 * @since 3.1
 * @version 1.0
 */
struct HdfLightEffect {
    int lightBrightness;    /**< Light brightness. Bits 0 to 7 stand for blue, bits 8 to 15 for green, bits 16 to 23 for red, and bits 24 to 31 are reserved. If the byte segment is 0,
                                 the default brightness set in the HCS will be used. */
    struct HdfLightFlashEffect flashEffect;    /**< Blinking parameters. For details, see {@link LightFlashEffect}. */
};
/** @} */
