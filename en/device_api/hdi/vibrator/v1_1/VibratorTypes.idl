/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Vibrator
 * @{
 *
 * @brief Provides APIs for vibrator services to access the vibrator driver.
 *
 * After obtaining a driver object or agent, a vibrator service starts or stops the vibrator or setting
 * intensity and frequency, using the APIs provided by the driver object or agent.
 *
 * @since 3.2
 * @version 1.1
 */

/**
 * @file VibratorTypes.idl
 *
 * @brief Declares the vibrator data structure, including the vibration mode and effect.
 *
 * @since 3.2
 * @version 1.1
 */

/**
 * @brief Defines the package path of the Vibrator module APIs.
 *
 * @since 3.2
 * @version 1.1
 */
package ohos.hdi.vibrator.v1_1;

/**
 * @brief Enumerates the vibration modes of this vibrator.
 *
 * @since 2.2
 * @version 1.0
 */
enum HdfVibratorMode {
    HDF_VIBRATOR_MODE_ONCE,     /**< A one-shot vibration with the given duration. */
    HDF_VIBRATOR_MODE_PRESET,   /**< A periodic vibration with the preset effect. */
    HDF_VIBRATOR_MODE_BUTT,     /**< Invalid mode. */
};

/**
 * @brief Defines the vibration parameters.
 *
 * The parameters include the support for the vibrator intensity and frequency settings as well as the range of the intensity and frequency.
 *
 * @since 3.2
 * @version 1.1
 */
struct HdfVibratorInfo {
    int isSupportIntensity;     /**< Whether the intensity setting is supported. The value 1 means that the setting is supported,
                                     and 0 means the opposite. */
    int isSupportFrequency;     /**< Whether the frequency setting is supported. The value 1 means that the setting is supported,
                                     and 0 means the opposite. */
    int intensityMaxValue;      /**< Max intensity. */
    int intensityMinValue;      /**< Min intensity. */
    int frequencyMaxValue;      /**< Max frequency. */
    int frequencyMinValue;      /**< Min frequency. */
};
/** @} */
