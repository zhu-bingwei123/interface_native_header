/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Distributed Camera
 * @{
 *
 * @brief Provides APIs for the Distributed Camera module.
 *
 * You can use the APIs which are consistent with camera APIs to perform operations on
 * distributed camera devices and streams, and implement various callbacks.
 * Communicate with Source SA through IDCameraProvider to achieve distributed functionality.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IDCameraProvider.idl
 *
 * @brief Transfer interfaces call between distributed camera SA service and distributed camera HDF service,
 * and provide Hardware Driver Interfaces (HDIs) for the upper layer.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the distributed camera module APIs.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.distributed_camera.v1_0;

import ohos.hdi.distributed_camera.v1_0.DCameraTypes;
import ohos.hdi.distributed_camera.v1_0.IDCameraProviderCallback;

/**
 * @brief Defines the basic operations available for distributed camera devices.
 *
 * You can set Enable distributed camera devices, set stream processing, update control parameters,
 * execute metadata and other related operations.
 */
interface IDCameraProvider {
    /**
     * @brief Enable distributed camera device and set callback. For details about the callbacks,
     * see {@link IDCameraProviderCallback}.
     *
     * @param dhBase Distributed hardware device base info.
     * @param abilityInfo The static capability info of the distributed camera device to be enabled.
     * @param callbackObj Indicates the callbacks to set.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful,
     * @return an error code defined in {@link DCamRetCode} otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    EnableDCameraDevice([in] struct DHBase dhBase,[in] String abilityInfo,[in] IDCameraProviderCallback callbackObj);

    /**
     * @brief Disable distributed camera device.
     *
     * @param dhBase Distributed hardware device base info.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful,
     * @return an error code defined in {@link DCamRetCode} otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    DisableDCameraDevice([in] struct DHBase dhBase);

    /**
     * @brief Acquire a frame buffer from the procedure handle which attached to the streamId.
     *
     * @param dhBase Distributed hardware device base info.
     * @param streamId Indicates the ID of the stream to which the procedure handle is to be attached.
     * @param buffer A frame buffer.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful,
     * @return an error code defined in {@link DCamRetCode} otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    AcquireBuffer([in] struct DHBase dhBase,[in] int streamId,[out] struct DCameraBuffer buffer);

    /**
     * @brief Notify distributed camera HDF service when a frame buffer has been filled.
     *
     * @param dhBase Distributed hardware device base info.
     * @param streamId Indicates the ID of the stream to which the frame buffer is to be attached.
     * @param buffer output frame buffer.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful,
     * @return an error code defined in {@link DCamRetCode} otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    ShutterBuffer([in] struct DHBase dhBase,[in] int streamId,[in] struct DCameraBuffer buffer);

    /**
     * @brief Called to report metadata related to the distributed camera device.
     *
     * @param dhBase Distributed hardware device base info.
     * @param result Indicates the metadata reported.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful,
     * @return an error code defined in {@link DCamRetCode} otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    OnSettingsResult([in] struct DHBase dhBase,[in] struct DCameraSettings result);

    /**
     * @brief Called to notify some events from distributed camera SA service to distributed camera HDF service.
     *
     * @param dhBase Distributed hardware device base info.
     * @param event Detail event contents.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful,
     * @return an error code defined in {@link DCamRetCode} otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    Notify([in] struct DHBase dhBase,[in] struct DCameraHDFEvent event);
}
