/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup CodecBase
 * @{
 *
 * @brief Provides the common structs, character constants, and enums for running <b>OH_AVCodec</b> instances.
 * 
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 *
 * @since 9
 * @version 1.0
 */

/**
 * @file native_avcodec_base.h
 *
 * @brief Declares the common structs, character constants, and enums for running <b>OH_AVCodec</b> instances.
 *
 * @since 9
 * @version 1.0
 */

#ifndef NATIVE_AVCODEC_BASE_H
#define NATIVE_AVCODEC_BASE_H

#include <stdint.h>
#include <stdio.h>
#include "native_averrors.h"
#include "native_avformat.h"
#include "native_avmemory.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct NativeWindow OHNativeWindow;
typedef struct OH_AVCodec OH_AVCodec;

/**
 * @brief Enumerates the buffer flags of an <b>OH_AVCodec</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 9
 * @version 1.0
 */
typedef enum OH_AVCodecBufferFlags {
    AVCODEC_BUFFER_FLAGS_NONE = 0,
    /** The buffer contains an end-of-stream frame. */
    AVCODEC_BUFFER_FLAGS_EOS = 1 << 0,
    /** The buffer contains a sync frame. */
    AVCODEC_BUFFER_FLAGS_SYNC_FRAME = 1 << 1,
    /** The buffer contains part of a frame. */
    AVCODEC_BUFFER_FLAGS_INCOMPLETE_FRAME = 1 << 2,
    /** The buffer contains codec-specific data. */
    AVCODEC_BUFFER_FLAGS_CODEC_DATA = 1 << 3,
} OH_AVCodecBufferFlags;

/**
 * @brief Defines the buffer attributes of an <b>OH_AVCodec</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 9
 * @version 1.0
 */
typedef struct OH_AVCodecBufferAttr {
    /** Presentation timestamp of the buffer, in microseconds. */
    int64_t pts;
    /** Size of the data contained in the buffer, in bytes. */
    int32_t size;
    /** Start offset of valid data in the buffer. */
    int32_t offset;
    /** Buffer flag, which is a combination of multiple {@link OH_AVCodecBufferFlags}. */
    uint32_t flags;
} OH_AVCodecBufferAttr;

/**
 * @brief Defines the function pointer that is called to report error information when an error occurs 
 * during the running of an <b>OH_AVCodec</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @param codec Indicates the pointer to an <b>OH_AVCodec</b> instance.
 * @param errorCode Indicates an error code.
 * @param userData Indicates the pointer to user-specific data.
 * @since 9
 * @version 1.0
 */
typedef void (*OH_AVCodecOnError)(OH_AVCodec *codec, int32_t errorCode, void *userData);

/**
 * @brief Defines the function pointer that is called to report the attributes of the new stream
 * when the output stream changes. Note that the lifecycle of the pointer to the <b>OH_AVFormat</b> instance
 * is valid only when the function pointer is being called.
 * Do not access the pointer to the instance after the function pointer is called.
 * 
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @param codec Indicates the pointer to an <b>OH_AVCodec</b> instance.
 * @param format Indicates the handle to the attributes of the new output stream.
 * @param userData Indicates the pointer to user-specific data.
 * @since 9
 * @version 1.0
 */
typedef void (*OH_AVCodecOnStreamChanged)(OH_AVCodec *codec, OH_AVFormat *format, void *userData);

/**
 * @brief Defines the function pointer that is called, with a new buffer to fill in new input data,
 * when new input data is required during the running of an <b>OH_AVCodec</b> instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @param codec Indicates the pointer to an <b>OH_AVCodec</b> instance.
 * @param index Indicates the index of an input buffer.
 * @param data Indicates the pointer to the new input data.
 * @param userData Indicates the pointer to user-specific data.
 * @since 9
 * @version 1.0
 */
typedef void (*OH_AVCodecOnNeedInputData)(OH_AVCodec *codec, uint32_t index, OH_AVMemory *data, void *userData);

/**
 * @brief Defines the function pointer that is called, with a buffer containing new output data, 
 * when the new output data is generated during the running of an <b>OH_AVCodec</b> instance.
 * Note that the lifecycle of the pointer to the <b>OH_AVCodecBufferAttr</b> instance is valid
 * only when the function pointer is being called. Do not access the pointer to the instance 
 * after the function pointer is called.
 * 
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @param codec Indicates the pointer to an <b>OH_AVCodec</b> instance.
 * @param index Indicates the index of a new output buffer.
 * @param data Indicates the pointer to the new output data.
 * @param attr Indicates the pointer to the attributes of the new output buffer. For details, see {@link OH_AVCodecBufferAttr}. 
 * @param userData Indicates the pointer to user-specific data.
 * @param userData specified data
 * @since 9
 * @version 1.0
 */
typedef void (*OH_AVCodecOnNewOutputData)(OH_AVCodec *codec, uint32_t index, OH_AVMemory *data,
    OH_AVCodecBufferAttr *attr, void *userData);

/**
 * @brief Defines a collection of asynchronous callback functions for an <b>OH_AVCodec</b> instance. 
 * You must register this struct instance for an <b>OH_AVCodec</b> instance and process the information
 * reported through these callbacks to ensure the normal running of the instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @param onError Indicates the callback used to report errors occurred during the running of the instance.
 * For details, see {@link OH_AVCodecOnError}.
 * @param onStreamChanged Indicates the callback used to report stream information. 
 * For details, see {@link OH_AVCodecOnStreamChanged}.
 * @param onNeedInputData Indicates the callback used to report input data needed.
 * For details, see {@link OH_AVCodecOnNeedInputData}.
 * @param onNeedInputData Indicates the callback used to report output data needed.
 * For details, see {@link OH_AVCodecOnNewOutputData}.
 * @since 9
 * @version 1.0
 */
typedef struct OH_AVCodecAsyncCallback {
    OH_AVCodecOnError onError;
    OH_AVCodecOnStreamChanged onStreamChanged;
    OH_AVCodecOnNeedInputData onNeedInputData;
    OH_AVCodecOnNewOutputData onNeedOutputData;
} OH_AVCodecAsyncCallback;

/**
 * @brief Defines the Multipurpose Internet Mail Extension (MIME) type for Advanced Video Coding (AVC).
 * 
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 9
 * @version 1.0
 */
extern const char *OH_AVCODEC_MIMETYPE_VIDEO_AVC;

/**
 * @brief Defines the MIME type for Advanced Audio Coding (AAC).
 * 
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 9
 * @version 1.0
 */
extern const char *OH_AVCODEC_MIMETYPE_AUDIO_AAC;

/**
 * @brief Provides unified character descriptors for the auxiliary data of the surface buffer.
 * 
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 9
 * @version 1.0
 */
/** Character descriptor of the timestamp in the surface auxiliary data. The value type is int64. */
extern const char *OH_ED_KEY_TIME_STAMP;
/** Character descriptor of the end-of-stream in the surface auxiliary data. The value type is bool. */
extern const char *OH_ED_KEY_EOS;

/**
 * @brief Provides unified character descriptors for the media playback framework.
 * 
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 9
 * @version 1.0
 */
/** Character descriptor of the track type. The value type is uint8_t. For details, see {@link OH_MediaType}. */
extern const char *OH_MD_KEY_TRACK_TYPE;
/** Character descriptor of the MIME type. The value type is string. */
extern const char *OH_MD_KEY_CODEC_MIME;
/** Character descriptor of duration. The value type is int64_t. */
extern const char *OH_MD_KEY_DURATION;
/** Character descriptor of the bit rate. The value type is uint32_t. */
extern const char *OH_MD_KEY_BITRATE;
/** Character descriptor of the maximum input size. The value type is uint32_t. */
extern const char *OH_MD_KEY_MAX_INPUT_SIZE;
/** Character descriptor of the video width. The value type is uint32_t. */
extern const char *OH_MD_KEY_WIDTH;
/** Character descriptor of the video height. The value type is uint32_t. */
extern const char *OH_MD_KEY_HEIGHT;
/** Character descriptor of the video pixel format. The value type is int32_t. For details, see {@link OH_AVPixelFormat}. */
extern const char *OH_MD_KEY_PIXEL_FORMAT;
/** Character descriptor of the audio sample format. The value type is uint32_t. */
extern const char *OH_MD_KEY_AUDIO_SAMPLE_FORMAT;
/** Character descriptor of the video frame rate. The value type is double. */
extern const char *OH_MD_KEY_FRAME_RATE;
/** Character descriptor of the video encoding bit rate mode. The value type is int32_t. For details, see {@link OH_VideoEncodeBitrateMode}. */
extern const char *OH_MD_KEY_VIDEO_ENCODE_BITRATE_MODE;
/** Character descriptor of the audio/video encoding capability. The value type is int32_t. For details, see {@link OH_AVCProfile} or {@link OH_AACProfile}. */
extern const char *OH_MD_KEY_PROFILE;
/** Character descriptor of the number of audio channels. The value type is uint32_t. */
extern const char *OH_MD_KEY_AUD_CHANNEL_COUNT;
/** Character descriptor of the audio sampling rate. The value type is uint32_t. */
extern const char *OH_MD_KEY_AUD_SAMPLE_RATE;
/** Character descriptor of the I-frame interval. The value type is int32_t, and the unit is ms. */
extern const char *OH_MD_KEY_I_FRAME_INTERVAL;
/** Character descriptor of the surface rotation angle. The value type is int32_t. The value range is {0, 90, 180, 270}. The default value is 0. */
extern const char *OH_MD_KEY_ROTATION;

/**
 * @brief Enumerates the media types.
 * 
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 9
 * @version 1.0
 */
typedef enum OH_MediaType {
    /** Audio track. */
    MEDIA_TYPE_AUD = 0,
    /** Video track. */
    MEDIA_TYPE_VID = 1,
} OH_MediaType;

/**
 * @brief Enumerates the AVC profiles.
 * 
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 9
 * @version 1.0
 */
typedef enum OH_AVCProfile {
    AVC_PROFILE_BASELINE = 0,
    AVC_PROFILE_HIGH = 4,
    AVC_PROFILE_MAIN = 8,
} OH_AVCProfile;

/**
 * @brief Enumerates the AAC profiles.
 * 
 * @syscap SystemCapability.Multimedia.Media.CodecBase
 * @since 9
 * @version 1.0
 */
typedef enum OH_AACProfile {
    AAC_PROFILE_LC = 0,
} OH_AACProfile;

#ifdef __cplusplus
}
#endif

#endif // NATIVE_AVCODEC_BASE_H
