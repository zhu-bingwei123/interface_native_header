/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup image
 * @{
 *
 * @brief 提供图片编解码能力。
 *
 * @since 12
 */

/**
 * @file pixelmap_native.h
 *
 * @brief 访问Pixelmap的API。
 *
 * @library libpixelmap.so
 * @Syscap SystemCapability.Multimedia.Image.Core
 * @since 12
 */

#ifndef INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_PIXELMAP_NATIVE_H_
#define INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_PIXELMAP_NATIVE_H_
#include "image_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Pixelmap结构体类型，用于执行Pixelmap相关操作。
 *
 * @since 12
 */
struct OH_PixelmapNative;
typedef struct OH_PixelmapNative OH_PixelmapNative;

/**
 * @brief Pixelmap透明度类型。
 *
 * @since 12
 */
typedef enum {
    /*
    * 未知格式
    */
    PIXELMAP_ALPHA_TYPE_UNKNOWN = 0,
     /*
    * 不透明的格式
    */
    PIXELMAP_ALPHA_TYPE_OPAQUE = 1,
     /*
    * 预乘透明度格式
    */
    PIXELMAP_ALPHA_TYPE_PREMULTIPLIED = 2,
}PIXELMAP_ALPHA_TYPE;

typedef enum {
    /*
    * 未知格式
    */
    PIXEL_FORMAT_UNKNOWN = 0,
    /*
    * RGB_565格式
    */
    PIXEL_FORMAT_RGB_565 = 2,
    /*
    * RGBA_8888格式
    */
    PIXEL_FORMAT_RGBA_8888 = 3,
    /*
    * BGRA_8888格式
    */
    PIXEL_FORMAT_BGRA_8888 = 4,
    /*
    * RGB_888格式
    */
    PIXEL_FORMAT_RGB_888 = 5,
    /*
    * ALPHA_8格式
    */
    PIXEL_FORMAT_ALPHA_8 = 6,
    /*
    * RGBA_F16格式
    */
    PIXEL_FORMAT_RGBA_F16 = 7,
    /*
    * NV21格式
    */
    PIXEL_FORMAT_NV21 = 8,
    /*
    * NV12格式
    */
    PIXEL_FORMAT_NV12 = 9,
} PIXEL_FORMAT;

/**
 * @brief 初始化参数结构体。
 *
 * @since 12
 */
struct OH_Pixelmap_InitializationOptions;
typedef struct OH_Pixelmap_InitializationOptions OH_Pixelmap_InitializationOptions;

/**
 * @brief 创建OH_Pixelmap_InitializationOptions指针。
 *
 * @param options 被创建的OH_Pixelmap_InitializationOptions指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_Create(OH_Pixelmap_InitializationOptions **options);

/**
 * @brief 获取图片宽。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param width 图片的宽，单位：像素。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_GetWidth(OH_Pixelmap_InitializationOptions *options,
    uint32_t *width);

/**
 * @brief 设置图片宽。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param width 图片的宽，单位：像素。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_SetWidth(OH_Pixelmap_InitializationOptions *options,
    uint32_t width);

/**
 * @brief 获取图片高。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param height 图片的高，单位：像素。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_GetHeight(OH_Pixelmap_InitializationOptions *options,
    uint32_t *height);

/**
 * @brief 设置图片高。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param height 图片的高，单位：像素。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_SetHeight(OH_Pixelmap_InitializationOptions *options,
    uint32_t height);

/**
 * @brief 获取像素格式。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param pixelFormat 像素格式{@link PIXEL_FORMAT}。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_GetPixelFormat(OH_Pixelmap_InitializationOptions *options,
    int32_t *pixelFormat);

/**
 * @brief 设置像素格式。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param pixelFormat 像素格式{@link PIXEL_FORMAT}。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_SetPixelFormat(OH_Pixelmap_InitializationOptions *options,
    int32_t pixelFormat);

/**
 * @brief 获取透明度类型。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param alphaType 透明度类型{@link PIXELMAP_ALPHA_TYPE}。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_GetAlphaType(OH_Pixelmap_InitializationOptions *options,
    int32_t *alphaType);

/**
 * @brief 设置透明度类型。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param alphaType 透明度类型{@link PIXELMAP_ALPHA_TYPE}。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_SetAlphaType(OH_Pixelmap_InitializationOptions *options,
    int32_t alphaType);

/**
 * @brief 释放OH_Pixelmap_InitializationOptions指针。
 *
 * @param options 被释放的OH_Pixelmap_InitializationOptions指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_Release(OH_Pixelmap_InitializationOptions *options);

/**
 * @brief 图像像素信息结构体。
 *
 * @since 12
 */
struct OH_Pixelmap_ImageInfo;
typedef struct OH_Pixelmap_ImageInfo OH_Pixelmap_ImageInfo;

/**
 * @brief 创建OH_Pixelmap_ImageInfo指针。
 *
 * @param info 被创建的OH_Pixelmap_ImageInfo指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapImageInfo_Create(OH_Pixelmap_ImageInfo **info);

/**
 * @brief 获取图片宽。
 *
 * @param info 被操作的OH_Pixelmap_ImageInfo指针。
 * @param width 图片宽，单位：像素。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapImageInfo_GetWidth(OH_Pixelmap_ImageInfo *info, uint32_t *width);

/**
 * @brief 获取图片高。
 *
 * @param info 被操作的OH_Pixelmap_ImageInfo指针。
 * @param height 图片高，单位：像素。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapImageInfo_GetHeight(OH_Pixelmap_ImageInfo *info, uint32_t *height);

/**
 * @brief 获取行跨距。
 *
 * @param info 被操作的OH_Pixelmap_ImageInfo指针。
 * @param rowStride 跨距，内存中每行像素所占的空间。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapImageInfo_GetRowStride(OH_Pixelmap_ImageInfo *info, uint32_t *rowStride);

/**
 * @brief 获取像素格式。
 *
 * @param info 被操作的OH_Pixelmap_ImageInfo指针。
 * @param pixelFormat 像素格式。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapImageInfo_GetPixelFormat(OH_Pixelmap_ImageInfo *info, int32_t *pixelFormat);

/**
 * @brief 获取透明度类型。
 *
 * @param info 被操作的OH_Pixelmap_ImageInfo指针。
 * @param alphaType 透明度类型{@link PIXELMAP_ALPHA_TYPE}。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapImageInfo_GetAlphaType(OH_Pixelmap_ImageInfo *info, int32_t *alphaType);

/**
 * @brief 获取Pixelmap是否为高动态范围的信息。
 *
 * @param info 被操作的OH_Pixelmap_ImageInfo指针。
 * @param isHdr 是否为hdr的布尔值。
 * @return 如果操作成功返回IMAGE_SUCCESS，参数校验错误返回IMAGE_BAD_PARAMETER。具体请参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapImageInfo_GetDynamicRange(OH_Pixelmap_ImageInfo *info, bool *isHdr);

/**
 * @brief 释放OH_Pixelmap_ImageInfo指针。
 *
 * @param info 被释放的OH_Pixelmap_ImageInfo指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapImageInfo_Release(OH_Pixelmap_ImageInfo *info);

/**
 * @brief 通过属性创建PixelMap，默认采用BGRA_8888格式处理数据。
 *
 * @param data BGRA_8888格式的颜色数组。
 * @param dataLength 数组长度。
 * @param options 创建像素的属性。
 * @param pixelmap 被创建的OH_PixelmapNative对象指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果不支持的操作返回 IMAGE_UNSUPPORTED_OPERATION，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_CreatePixelmap(uint8_t *data, size_t dataLength,
    OH_Pixelmap_InitializationOptions *options, OH_PixelmapNative **pixelmap);

/**
 * @brief 读取图像像素数据，结果写入ArrayBuffer里，使用Promise形式返回。
 * 指定BGRA_8888格式创建pixelmap，读取的像素数据与原数据保持一致。
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param destination 缓冲区，获取的图像像素数据写入到该内存区域内。
 * @param bufferSize 缓冲区大小。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果未知错误返回 IMAGE_UNKNOWN_ERROR，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_ReadPixels(OH_PixelmapNative *pixelmap, uint8_t *destination, size_t *bufferSize);

/**
 * @brief 读取缓冲区中的图片数据，结果写入PixelMap中
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param source 图像像素数据。
 * @param bufferSize 图像像素数据长度。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果不支持的操作返回 IMAGE_UNSUPPORTED_OPERATION，如果未知错误返回 IMAGE_UNKNOWN_ERROR，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_WritePixels(OH_PixelmapNative *pixelmap, uint8_t *source, size_t bufferSize);

/**
 * @brief 获取图像像素信息。
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param imageInfo 图像像素信息。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_GetImageInfo(OH_PixelmapNative *pixelmap, OH_Pixelmap_ImageInfo *imageInfo);

/**
 * @brief 通过设置透明比率来让PixelMap达到对应的透明效果
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param rate 透明比率的值。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_Opacity(OH_PixelmapNative *pixelmap, float rate);

/**
 * @brief 根据输入的宽高对图片进行缩放。
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param scaleX 宽度的缩放比例。
 * @param scaleY 高度的缩放比例。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_Scale(OH_PixelmapNative *pixelmap, float scaleX, float scaleY);

/**
 * @brief 根据输入的坐标对图片进行位置变换。
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param x 区域横坐标。
 * @param y 区域纵坐标。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_Translate(OH_PixelmapNative *pixelmap, float x, float y);

/**
 * @brief 根据输入的角度对图片进行旋转。
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param angle 图片旋转的角度，单位为deg。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_Rotate(OH_PixelmapNative *pixelmap, float angle);

/**
 * @brief 根据输入的条件对图片进行翻转。
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param shouldFilpHorizontally 是否水平翻转图像。
 * @param shouldFilpVertically 是否垂直翻转图像。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_Flip(OH_PixelmapNative *pixelmap, bool shouldFilpHorizontally, bool shouldFilpVertically);

/**
 * @brief 根据输入的尺寸对图片进行裁剪
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param region 裁剪的尺寸。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_Crop(OH_PixelmapNative *pixelmap, Image_Region *region);

/**
 * @brief 释放OH_PixelmapNative指针。
 *
 * @param pixelmap 被释放的OH_PixelmapNative指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_Release(OH_PixelmapNative *pixelmap);


#ifdef __cplusplus
};
#endif
/** @} */
#endif // INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_PIXELMAP_NATIVE_H_
