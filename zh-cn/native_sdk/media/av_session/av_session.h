/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_AVSESSION_H
#define OHOS_AVSESSION_H

/**
 * @addtogroup avsession
 * @{
 *
 * @brief 音视频媒体会话，提供系统内媒体的统一控制能力。
 *
 * 功能包括媒体会话，媒体会话管理，媒体会话控制。
 *
 * @syscap SystemCapability.Multimedia.AVSession.Core
 * @since 9
 * @version 1.0
 */

/**
 * @file av_session.h
 *
 * @brief 会话的设置、获取等声明。
 *
 * @since 9
 * @version 1.0
 */

#include <string>
#include <memory>

#include "avsession_info.h"
#include "want_agent.h"
#include "avsession_controller.h"

namespace OHOS::AVSession {
/**
 * @brief 会话对象，支持配置会话属性，并可主动更新播放状态和会话元数据。
 */
class AVSession {
public:

    /**
     * @brief 会话类型的枚举。
     *
     * @since 9
     * @version 1.0
     */
    enum {
        /** 无效会话 */
        SESSION_TYPE_INVALID = -1,
        /** 音频会话 */
        SESSION_TYPE_AUDIO = 0,
        /** 视频会话 */
        SESSION_TYPE_VIDEO = 1
    };

    /**
     * @brief 获取会话的标识。
     *
     * @return 返回会话的标识。
     * @since 9
     * @version 1.0
     */
    virtual std::string GetSessionId() = 0;

    /**
     * @brief 获取会话元数据。
     *
     * @param meta 用于保存会话的元数据{@link AVMetaData}对象。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @see SetAVMetaData
     * @since 9
     * @version 1.0
     */
    virtual int32_t GetAVMetaData(AVMetaData& meta) = 0;

    /**
     * @brief 设置会话元数据。
     *
     * @param meta 用于修改会话的元数据{@link AVMetaData}对象。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @see GetAVMetaData
     * @since 9
     * @version 1.0
     */
    virtual int32_t SetAVMetaData(const AVMetaData& meta) = 0;

    /**
     * @brief 获取音视频的播放状态。
     *
     * @param state 用于保存播放状态的{@link AVPlaybackState}对象。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @see SetAVPlaybackState
     * @since 9
     * @version 1.0
     */
    virtual int32_t GetAVPlaybackState(AVPlaybackState& state) = 0;

    /**
     * @brief 设置音视频的播放状态。
     *
     * @param state 用于修改播放状态的{@link AVPlaybackState}对象。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @see GetAVPlaybackState
     * @since 9
     * @version 1.0
     */
    virtual int32_t SetAVPlaybackState(const AVPlaybackState& state) = 0;

    /**
     * @brief 设置一个WantAgent用于启动会话的Ability。
     *
     * @param ability 具体的应用对应的能力，类型为{@link AbilityRuntime::WantAgent::WantAgent}。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @see AVSessionController#GetLaunchAbility
     * @since 9
     * @version 1.0
     */
    virtual int32_t SetLaunchAbility(const AbilityRuntime::WantAgent::WantAgent& ability) = 0;

    /**
     * @brief 获取会话控制器。
     *
     * @return 返回会话控制器，{@link AVSessionController}类型智能指针。
     * @since 9
     * @version 1.0
     */
    virtual std::shared_ptr<AVSessionController> GetController() = 0;

    /**
     * @brief 注册会话回调。
     *
     * @param callback 用于注册会话回调的{@link AVSessionCallback}对象。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @since 9
     * @version 1.0
     */
    virtual int32_t RegisterCallback(const std::shared_ptr<AVSessionCallback>& callback) = 0;

    /**
     * @brief 激活会话。
     *
     * 激活成功后，会话才可以接收控制指令。
     *
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @see Deactivate
     * @see IsActive
     * @since 9
     * @version 1.0
     */
    virtual int32_t Activate() = 0;

    /**
     * @brief 去激活会话。
     *
     * 去激活成功后，表示会话还不能接收控制指令。
     *
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @see Activate
     * @see IsActive
     * @since 9
     * @version 1.0
     */
    virtual int32_t Deactivate() = 0;

    /**
     * @brief 获取会话是否被激活。
     *
     * @return 如果是激活状态，则返回true；否则返回false。
     * @see Activate
     * @see Deactivate
     * @since 9
     * @version 1.0
     */
    virtual bool IsActive() = 0;

    /**
     * @brief 销毁会话。
     *
     * 如果应用要创建一个新会话，必须要销毁之前的会话，否则会创建失败。
     *
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @since 9
     * @version 1.0
     */
    virtual int32_t Destroy() = 0;

    /**
     * @brief 添加支持的控制命令。
     *
     * @param cmd 待添加的控制命令，范围为{@link #SESSION_CMD_INVALID}到{@link #SESSION_CMD_MAX}。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @since 9
     * @version 1.0
     */
    virtual int32_t AddSupportCommand(const int32_t cmd) = 0;

    /**
     * @brief 删除支持的控制命令。
     *
     * @param cmd 待删除的控制命令，范围为{@link #SESSION_CMD_INVALID}到{@link #SESSION_CMD_MAX}。
     * @return 成功返回{@link #AVSESSION_SUCCESS}；失败则返回对应错误码。
     * @since 9
     * @version 1.0
     */
    virtual int32_t DeleteSupportCommand(const int32_t cmd) = 0;

    virtual ~AVSession() = default;
};
} // namespace OHOS::AVSession
/** @} */
#endif // OHOS_AVSESSION_H