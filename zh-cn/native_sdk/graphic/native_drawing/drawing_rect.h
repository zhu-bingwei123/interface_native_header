/*
 * Copyright (c) 2023-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_RECT_H
#define C_INCLUDE_DRAWING_RECT_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_rect.h
 *
 * @brief 文件中定义了与矩形相关的功能函数。
 *
 * 引用文件"native_drawing/drawing_rect.h"
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 用于创建一个矩形对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param left 矩形左上角的横坐标。
 * @param top 矩形左上角的纵坐标。
 * @param right 矩形右下角的横坐标。
 * @param bottom 矩形右下角的纵坐标。
 * @return 函数会返回一个指针，指针指向创建的矩形对象。
 * @since 11
 * @version 1.0
 */
OH_Drawing_Rect* OH_Drawing_RectCreate(float left, float top, float right, float bottom);

/**
 * @brief 用于获取矩形对象高度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Rect 指向矩形对象的指针。
 * @return 返回矩形对象的高度。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_RectGetHeight(OH_Drawing_Rect*);

/**
 * @brief 用于获取矩形对象的宽度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Rect 指向矩形对象的指针。
 * @return 返回矩形对象的宽度。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_RectGetWidth(OH_Drawing_Rect*);

/**
 * @brief 用于获取矩形左上角的横坐标。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Rect 指向矩形对象的指针。
 * @return 矩形左上角的横坐标。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_RectGetLeft(OH_Drawing_Rect*);

/**
 * @brief 用于获取矩形左上角的纵坐标。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Rect 指向矩形对象的指针。
 * @return 矩形左上角的纵坐标。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_RectGetTop(OH_Drawing_Rect*);

/**
 * @brief 用于获取矩形右下角的横坐标。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Rect 指向矩形对象的指针。
 * @return 矩形右下角的横坐标。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_RectGetRight(OH_Drawing_Rect*);

/**
 * @brief 用于获取矩形右下角的纵坐标。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Rect 指向矩形对象的指针。
 * @return 矩形右下角的纵坐标。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_RectGetBottom(OH_Drawing_Rect*);

/**
 * @brief 用于判断两个矩形是否相交。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param rect 指向矩形对象的指针。
 * @param other 指向矩形对象的指针。
 * @return 返回两个矩形是否相交的结果。true表示这两个矩形相交，false则表示不相交。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_RectIntersect(OH_Drawing_Rect* rect, const OH_Drawing_Rect* other);

/**
 * @brief 将两个矩形取并集。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param rect 指向矩形对象的指针。
 * @param other 指向矩形对象的指针。
 * @return 返回两个矩形取并集的结果。true表示成功，false表示失败，失败的原因可能是两个矩形至少有一个为NULL或者other矩形大小为空。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_RectJoin(OH_Drawing_Rect* rect, const OH_Drawing_Rect* other);

/**
 * @brief 用于设置矩形左上角的横坐标。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param rect 指向矩形对象的指针。
 * @param left 矩形左上角的横坐标。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_RectSetLeft(OH_Drawing_Rect* rect, float left);

/**
 * @brief 用于设置矩形左上角的纵坐标。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param rect 指向矩形对象的指针。
 * @param top 矩形左上角的纵坐标。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_RectSetTop(OH_Drawing_Rect* rect, float top);

/**
 * @brief 用于设置矩形右下角的横坐标。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param rect 指向矩形对象的指针。
 * @param right 矩形右下角的横坐标。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_RectSetRight(OH_Drawing_Rect* rect, float right);

/**
 * @brief 用于设置矩形右下角的纵坐标。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param rect 指向矩形对象的指针。
 * @param bottom 矩形右下角的纵坐标。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_RectSetBottom(OH_Drawing_Rect* rect, float bottom);

/**
 * @brief 用于将源矩形对象复制到目标矩形对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param sRect 指向源矩形对象的指针。
 * @param dRect 指向目标矩形对象的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_RectCopy(OH_Drawing_Rect* sRect, OH_Drawing_Rect* dRect);

/**
 * @brief 用于销毁矩形对象并回收该对象占有的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Rect 指向矩形对象的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_RectDestroy(OH_Drawing_Rect*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
