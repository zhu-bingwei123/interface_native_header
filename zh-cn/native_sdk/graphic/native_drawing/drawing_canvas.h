/*
 * Copyright (c) 2021-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_H
#define C_INCLUDE_DRAWING_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_canvas.h
 *
 * @brief 文件中定义了与画布相关的功能函数。
 *
 * 引用文件"native_drawing/drawing_canvas.h"
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 源矩形区域约束类型枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_SrcRectConstraint {
    /** 严格约束，源矩形区域必须完全包含在图像中 */
    STRICT_SRC_RECT_CONSTRAINT,
    /** 快速约束，源矩形区域可以部分位于图像之外 */
    FAST_SRC_RECT_CONSTRAINT,
} OH_Drawing_SrcRectConstraint;

/**
 * @brief 用于创建一个画布对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 函数会返回一个指针，指针指向创建的画布对象。
 * @since 8
 * @version 1.0
 */
OH_Drawing_Canvas* OH_Drawing_CanvasCreate(void);

/**
 * @brief 用于销毁画布对象并回收该对象占有的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDestroy(OH_Drawing_Canvas*);

/**
 * @brief 用于将一个位图对象绑定到画布中，使得画布绘制的内容输出到位图中（即CPU渲染）。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param OH_Drawing_Bitmap 指向位图对象的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasBind(OH_Drawing_Canvas*, OH_Drawing_Bitmap*);

/**
 * @brief 用于设置画笔给画布，画布将会使用设置画笔的样式和颜色去绘制图形形状的轮廓。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param OH_Drawing_Pen 指向画笔对象的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasAttachPen(OH_Drawing_Canvas*, const OH_Drawing_Pen*);

/**
 * @brief 用于去除掉画布中的画笔，使用后画布将不去绘制图形形状的轮廓。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDetachPen(OH_Drawing_Canvas*);

/**
 * @brief 用于设置画刷给画布，画布将会使用设置的画刷样式和颜色去填充绘制的图形形状。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param OH_Drawing_Brush 指向画刷对象的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasAttachBrush(OH_Drawing_Canvas*, const OH_Drawing_Brush*);

/**
 * @brief 用于去除掉画布中的画刷，使用后画布将不去填充图形形状。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDetachBrush(OH_Drawing_Canvas*);

/**
 * @brief 用于保存当前画布的状态（画布矩阵）到一个栈顶。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasSave(OH_Drawing_Canvas*);

/**
 * @brief 保存矩阵和裁剪区域，为后续绘制分配位图。调用恢复接口
 * {@link OH_Drawing_CanvasRestore}将放弃对矩阵和剪切区域所做的更改，并绘制位图。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象{@link OH_Drawing_Canvas}的指针。
 * @param OH_Drawing_Rect 指向矩形对象{@link OH_Drawing_Rect}的指针。
 * @param OH_Drawing_Brush 指向画刷对象{@link OH_Drawing_Brush}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasSaveLayer(OH_Drawing_Canvas*, const OH_Drawing_Rect*, const OH_Drawing_Brush*);

/**
 * @brief 用于恢复保存在栈顶的画布状态（画布矩阵）。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasRestore(OH_Drawing_Canvas*);

/**
 * @brief 用于获取栈中保存的画布状态（画布矩阵）的数量。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @return 函数会返回一个32位的值描述画布状态（画布矩阵）的数量。
 * @since 11
 * @version 1.0
 */
uint32_t OH_Drawing_CanvasGetSaveCount(OH_Drawing_Canvas*);

/**
 * @brief 用于恢复到指定数量的画布状态（画布矩阵）。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param saveCount 指定的画布状态（画布矩阵）数量。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasRestoreToCount(OH_Drawing_Canvas*, uint32_t saveCount);

/**
 * @brief 用于画一条直线段。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param x1 线段起始点的横坐标。
 * @param y1 线段起始点的纵坐标。
 * @param x2 线段结束点的横坐标。
 * @param y2 线段结束点的纵坐标。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDrawLine(OH_Drawing_Canvas*, float x1, float y1, float x2, float y2);

/**
 * @brief 用于画一个自定义路径。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param OH_Drawing_Path 指向路径对象的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDrawPath(OH_Drawing_Canvas*, const OH_Drawing_Path*);

/**
 * @brief 用于将像素图的指定区域绘制到画布的指定区域。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象{@link OH_Drawing_Canvas}的指针。
 * @param OH_Drawing_PixelMap 指向像素图{@link OH_Drawing_PixelMap}的指针。
 * @param src 源画布指定矩形区域，可以为空。
 * @param dst 目标画布指定矩形区域。
 * @param OH_Drawing_SamplingOptions 指向采样选项对象{@link OH_Drawing_SamplingOptions}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawPixelMapRect(OH_Drawing_Canvas*, OH_Drawing_PixelMap*, const OH_Drawing_Rect* src,
    const OH_Drawing_Rect* dst, const OH_Drawing_SamplingOptions*);

/**
 * @brief 用于画一个背景，此背景以画刷填充。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param OH_Drawing_Brush 指向画刷对象的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawBackground(OH_Drawing_Canvas*, const OH_Drawing_Brush*);

/**
 * @brief 用于画一块区域。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas  指向画布对象的指针。
 * @param OH_Drawing_Region  指向区域对象的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawRegion(OH_Drawing_Canvas*, const OH_Drawing_Region*);

/**
 * @brief 绘制多个点的方式枚举，方式分为离散点、直线或开放多边形。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_PointMode {
    /**
     * 分别绘制每个点。
     */
    POINT_MODE_POINTS,
    /**
     * 将每两个点绘制为线段。
     */
    POINT_MODE_LINES,
     /**
     * 将点阵列绘制为开放多边形。
     */
    POINT_MODE_POLYGON,
} OH_Drawing_PointMode;

/**
 * @brief 用于画多个点，绘制方式分为绘制单独的点、绘制成线段或绘制成开放多边形。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param mode 绘制多个点的方式，支持方式参考{@link OH_Drawing_PointMode}。
 * @param count 点的数量，即点数组中点的个数。
 * @param OH_Drawing_Point2D 指向多个点的数组。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawPoints(OH_Drawing_Canvas*, OH_Drawing_PointMode mode,
    uint32_t count, const OH_Drawing_Point2D*);

/**
 * @brief 用于画一个位图，位图又称为点阵图像、像素图或栅格图像，是由像素（图片元素）的单个点组成。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param OH_Drawing_Bitmap 指向位图对象的指针。
 * @param left 位图对象左上角的横坐标。
 * @param top 位图对象左上角的纵坐标。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawBitmap(OH_Drawing_Canvas*, const OH_Drawing_Bitmap*, float left, float top);

/**
 * @brief 将位图的指定区域绘制到画布的指定区域。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象{@link OH_Drawing_Canvas}的指针。
 * @param OH_Drawing_Bitmap 指向位图对象{@link OH_Drawing_Bitmap}的指针。
 * @param src 源位图指定矩形区域，可以为空。
 * @param dst 目标画布指定矩形区域。
 * @param OH_Drawing_SamplingOptions 指向采样选项对象{@link OH_Drawing_SamplingOptions}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawBitmapRect(OH_Drawing_Canvas*, const OH_Drawing_Bitmap*, const OH_Drawing_Rect* src,
    const OH_Drawing_Rect* dst, const OH_Drawing_SamplingOptions*);

/**
 * @brief 设置画布的矩阵状态。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象{@link OH_Drawing_Canvas}的指针。
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针，
 * 开发者可调用{@link OH_Drawing_MatrixCreate}接口创建。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasSetMatrix(OH_Drawing_Canvas*, OH_Drawing_Matrix*);

/**
 * @brief 重置当前画布的矩阵为单位矩阵。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象{@link OH_Drawing_Canvas}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasResetMatrix(OH_Drawing_Canvas*);

/**
 * @brief 将图片绘制到画布的指定区域上，源矩形选定的区域会缩放平移到目标矩形。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象{@link OH_Drawing_Canvas}的指针。
 * @param OH_Drawing_Image 指向图片对象{@link OH_Drawing_Image}的指针。
 * @param src 指向源矩形对象{@link OH_Drawing_Rect}的指针。
 * @param dst 指向目标矩形对象{@link OH_Drawing_Rect}的指针。
 * @param OH_Drawing_SamplingOptions 指向采样选项对象{@link OH_Drawing_SamplingOptions}的指针。
 * @param OH_Drawing_SrcRectConstraint 约束类型，支持可选的具体类型可见{@link OH_Drawing_SrcRectConstraint}枚举。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawImageRectWithSrc(OH_Drawing_Canvas*, const OH_Drawing_Image*,
    const OH_Drawing_Rect* src, const OH_Drawing_Rect* dst, const OH_Drawing_SamplingOptions*,
    OH_Drawing_SrcRectConstraint);

/**
 * @brief 将图片绘制到画布的指定区域上。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象{@link OH_Drawing_Canvas}的指针。
 * @param OH_Drawing_Image 指向图片对象{@link OH_Drawing_Image}的指针。
 * @param OH_Drawing_Rect 指向矩形对象{@link OH_Drawing_Rect}的指针。
 * @param OH_Drawing_SamplingOptions 指向采样选项对象{@link OH_Drawing_SamplingOptions}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawImageRect(OH_Drawing_Canvas*, OH_Drawing_Image*,
    OH_Drawing_Rect* dst, OH_Drawing_SamplingOptions*);

/**
 * @brief 用于指定如何解释给定顶点的几何形状的枚举类型。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_VertexMode {
    /**
     * 每三个顶点表示一个三角形，如果顶点数不是3的倍数，则多余的顶点会被忽略。
     */
    VERTEX_MODE_TRIANGLES,
    /**
     * 相邻三个顶点表示一个三角形，每个新的顶点将与前两个顶点组成一个新的三角形。
     */
    VERTEX_MODE_TRIANGLESSTRIP,
    /**
     * 第一个顶点作为中心点，后续的每个顶点都与前一个顶点和中心点组成一个三角形。
     */
    VERTEX_MODE_TRIANGLEFAN,
} OH_Drawing_VertexMode;

/**
 * @brief 用于画顶点数组描述的三角网格。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param vertexMmode 绘制顶点的枚举方式，支持方式参考{@link OH_Drawing_VertexMode}。
 * @param vertexCount 顶点数组元素的数量。
 * @param positions 定位数据数组。
 * @param texs 纹理坐标数据数组。
 * @param colors 颜色数据指针。
 * @param indexCount 索引数量。
 * @param indices 索引数据指针。
 * @param mode 混合模式枚举，支持方式参考{@link OH_Drawing_BlendMode}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawVertices(OH_Drawing_Canvas*, OH_Drawing_VertexMode vertexMmode,
    int32_t vertexCount, const OH_Drawing_Point2D* positions, const OH_Drawing_Point2D* texs,
    const uint32_t* colors, int32_t indexCount, const uint16_t* indices, OH_Drawing_BlendMode mode);

/**
 * @brief 从画布中拷贝像素数据到指定地址。该接口不可用于录制类型画布。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象{@link OH_Drawing_Canvas}的指针。
 * @param OH_Drawing_Image_Info 指向图片信息{@link OH_Drawing_Image_Info}的指针。
 * @param dstPixels 目标像素存储首地址。
 * @param dstRowBytes 一行像素的大小。
 * @param srcX 画布像素的x轴偏移量，单位为像素。
 * @param srcY 画布像素的y轴偏移量，单位为像素。
 * @return 函数返回true表示像素成功拷贝到目标像素存储首地址，函数返回false表示拷贝失败。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_CanvasReadPixels(OH_Drawing_Canvas*, OH_Drawing_Image_Info*,
    void* dstPixels, uint32_t dstRowBytes, int32_t srcX, int32_t srcY);

/**
 * @brief 从画布拷贝像素数据到位图中。该接口不可用于录制类型画布。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象{@link OH_Drawing_Canvas}的指针。
 * @param OH_Drawing_Bitmap 指向位图对象{@link OH_Drawing_Bitmap}的指针。
 * @param srcX 画布像素的x轴偏移量，单位为像素。
 * @param srcY 画布像素的y轴偏移量，单位为像素。
 * @return 函数返回true表示像素成功拷贝到位图，函数返回false表示拷贝失败。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_CanvasReadPixelsToBitmap(OH_Drawing_Canvas*, OH_Drawing_Bitmap*, int32_t srcX, int32_t srcY);

/**
 * @brief 用于画一个矩形。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param OH_Drawing_Rect 指向矩形对象的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawRect(OH_Drawing_Canvas*, const OH_Drawing_Rect*);

/**
 * @brief 用于画一个圆形。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param OH_Drawing_Point 指向坐标点对象的指针，表示圆心。
 * @param radius 圆形的半径。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawCircle(OH_Drawing_Canvas*, const OH_Drawing_Point*, float radius);

/**
 * @brief 用于画一个椭圆。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param OH_Drawing_Rect 指向矩形对象的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawOval(OH_Drawing_Canvas*, const OH_Drawing_Rect*);

/**
 * @brief 用于画一个弧。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param OH_Drawing_Rect 指向矩形对象的指针。
 * @param startAngle 弧的起始角度。
 * @param sweepAngle 弧的扫描角度。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawArc(OH_Drawing_Canvas*, const OH_Drawing_Rect*, float startAngle, float sweepAngle);

/**
 * @brief 用于画一个圆角矩形。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param OH_Drawing_RoundRect 指向圆角矩形对象的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawRoundRect(OH_Drawing_Canvas*, const OH_Drawing_RoundRect*);

/**
 * @brief 用于画一段文字。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param OH_Drawing_TextBlob 指向文本对象的指针。
 * @param x 文本对象左下角的横坐标。
 * @param y 文本对象左下角的纵坐标。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawTextBlob(OH_Drawing_Canvas*, const OH_Drawing_TextBlob*, float x, float y);

/**
 * @brief 画布裁剪方式的枚举集合。
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_CanvasClipOp {
    /**
     * 将指定区域裁剪（取差集）。
     */
    DIFFERENCE,
    /**
     * 将指定区域保留（取交集）。
     */
    INTERSECT,
} OH_Drawing_CanvasClipOp;

/**
 * @brief 用于裁剪一个矩形。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param OH_Drawing_Rect 指向矩形对象的指针。
 * @param clipOp 裁剪方式。支持可选的具体裁剪方式可见@{link OH_Drawing_CanvasClipOp}枚举。
 * @param doAntiAlias 值为true则做抗锯齿处理，反之不做。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasClipRect(OH_Drawing_Canvas*, const OH_Drawing_Rect*,
    OH_Drawing_CanvasClipOp clipOp, bool doAntiAlias);

/**
 * @brief 用于裁剪一个圆角矩形。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param OH_Drawing_RoundRect 指向圆角矩形对象的指针。
 * @param clipOp 裁剪方式。支持可选的具体裁剪方式可见@{link OH_Drawing_CanvasClipOp}枚举。
 * @param doAntiAlias 表示是否需要做抗锯齿处理，值为true时为需要，为false时为不需要。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasClipRoundRect(OH_Drawing_Canvas*, const OH_Drawing_RoundRect*,
    OH_Drawing_CanvasClipOp clipOp, bool doAntiAlias);

/**
 * @brief 用于裁剪一个自定义路径。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param OH_Drawing_Path 指向路径对象的指针。
 * @param clipOp 裁剪方式。支持可选的具体裁剪方式可见@{link OH_Drawing_CanvasClipOp}枚举。
 * @param doAntiAlias 真为抗锯齿，假则不做抗锯齿处理。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasClipPath(OH_Drawing_Canvas*, const OH_Drawing_Path*,
    OH_Drawing_CanvasClipOp clipOp, bool doAntiAlias);

/**
 * @brief 用于画布旋转一定的角度，正数表示顺时针旋转，负数反之。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param degrees 旋转角度。
 * @param px 旋转中心的横坐标。
 * @param py 旋转中心的纵坐标。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasRotate(OH_Drawing_Canvas*, float degrees, float px, float py);

/**
 * @brief 用于平移画布一段距离。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param dx x轴方向的移动距离。
 * @param dy y轴方向的移动距离。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasTranslate(OH_Drawing_Canvas*, float dx, float dy);

/**
 * @brief 用于画布缩放。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param sx x轴方向的缩放比例。
 * @param sy y轴方向的缩放比例。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasScale(OH_Drawing_Canvas*, float sx, float sy);

/**
 * @brief 用于画布倾斜变换，包括水平轴和垂直轴上的偏移。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param sx x轴上的倾斜量。为正值时画布会向右下方倾斜，为负值时画布会向左下方倾斜。
 * @param sy y轴上的倾斜量。为正值时画布会向右上方倾斜，为负值时画布会向左上方倾斜。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasSkew(OH_Drawing_Canvas*, float sx, float sy);

/**
 * @brief 用于使用指定颜色去清空画布。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象的指针。
 * @param color 描述颜色的32位（ARGB）变量。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasClear(OH_Drawing_Canvas*, uint32_t color);

/**
 * @brief 获取画布宽度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象{@link OH_Drawing_Canvas}的指针。
 * @return 函数返回画布宽度，单位为像素。
 * @since 12
 * @version 1.0
 */
int32_t OH_Drawing_CanvasGetWidth(OH_Drawing_Canvas*);

/**
 * @brief 获取画布高度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象{@link OH_Drawing_Canvas}的指针。
 * @return 函数返回画布高度，单位为像素。
 * @since 12
 * @version 1.0
 */
int32_t OH_Drawing_CanvasGetHeight(OH_Drawing_Canvas*);

/**
 * @brief 获取画布裁剪区域的边界。该接口不可用于录制类型画布。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象{@link OH_Drawing_Canvas}的指针。
 * @param OH_Drawing_Rect 指向矩形对象{@link OH_Drawing_Rect}的指针，
 * 开发者可调用{@link OH_Drawing_RectCreate}接口创建。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasGetLocalClipBounds(OH_Drawing_Canvas*, OH_Drawing_Rect*);

/**
 * @brief 获取画布3x3矩阵。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象{@link OH_Drawing_Canvas}的指针。
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针，
 * 开发者可调用{@link OH_Drawing_MatrixCreate}接口创建。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasGetTotalMatrix(OH_Drawing_Canvas*, OH_Drawing_Matrix*);

/**
 * @brief 画布现有矩阵左乘以传入矩阵，不影响该接口之前的绘制操作。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象{@link OH_Drawing_Canvas}的指针。
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasConcatMatrix(OH_Drawing_Canvas*, OH_Drawing_Matrix*);

/**
 * @brief 阴影标志枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_CanvasShadowFlags {
    /**
     * 无阴影标志。
     */
    SHADOW_FLAGS_NONE,
    /**
     * 遮挡物对象不透明标志。
     */
    SHADOW_FLAGS_TRANSPARENT_OCCLUDER,
    /**
     * 不分析阴影标志。
     */
    SHADOW_FLAGS_GEOMETRIC_ONLY,
    /**
     * 使能以上所有阴影标志。
     */
    SHADOW_FLAGS_ALL,
} OH_Drawing_CanvasShadowFlags;

/**
 * @brief 绘制射灯类型阴影，使用路径描述环境光阴影的轮廓。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas 指向画布对象{@link OH_Drawing_Canvas}的指针。
 * @param OH_Drawing_Path 指向路径对象{@link OH_Drawing_Path}的指针，用于生成阴影。
 * @param planeParams 基于x轴和y轴的遮挡器从画布返回z轴偏移量的函数值。
 * @param devLightPos 光线相对于画布的位置。
 * @param lightRadius 圆形灯半径。
 * @param ambientColor 环境阴影颜色。
 * @param spotColor 点阴影颜色。
 * @param flag 阴影标志枚举{@link OH_Drawing_CanvasShadowFlags}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawShadow(OH_Drawing_Canvas*, OH_Drawing_Path*, OH_Drawing_Point3D planeParams,
    OH_Drawing_Point3D devLightPos, float lightRadius, uint32_t ambientColor, uint32_t spotColor,
    OH_Drawing_CanvasShadowFlags flag);

#ifdef __cplusplus
}
#endif
/** @} */
#endif