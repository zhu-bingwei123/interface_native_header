/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FILEMANAGEMENT_APP_FILE_SERVICE_INTERFACES_FILE_SHARE_ERROR_CODE_H
#define FILEMANAGEMENT_APP_FILE_SERVICE_INTERFACES_FILE_SHARE_ERROR_CODE_H

/**
 * @addtogroup FileShare
 * @{
 *
 * @brief 提供文件基础操作的能力。
 * @since 12
 */

/**
 * @file error_code.h
 *
 * @brief 提供文件管理模块的错误码定义。
 * @syscap SystemCapability.FileManagement.File.FileShare
 * @since 12
 */

/**
 * @brief 文件管理模块错误码。
 * @since 12
 */
enum FileManagement_ErrCode {
    /**
     * 接口调用成功。
     */
    E_NO_ERROR = 0,
    /**
     * 接口权限校验失败。
     */
    E_PERMISSION = 201,
    /**
     * 错误参数。
     */
    E_PARAMS = 401,
    /**
     * 当前设备不支持此接口。
     */
    E_DEVICE_NOT_SUPPORT = 801,
    /**
     * 操作不被允许。
     */
    E_EPERM = 13900001,
    /**
     * 内存溢出。
     */
    E_ENOMEM = 13900011,
    /**
     * 内部未知错误。
     */
    E_UNKNOWN_ERROR = 13900042
};

#endif // FILEMANAGEMENT_APP_FILE_SERVICE_INTERFACES_FILE_SHARE_ERROR_CODE_H