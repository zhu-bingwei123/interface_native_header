/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiGeofence
 * @{
 *
 * @brief 定义GNSS地理围栏接口。
 *
 * 上层GNSS服务模块可以使用这个模块的接口来添加地理围栏，删除地理围栏，以及监视地理围栏状态的变化。
 *
 * @since 3.2
 */

/**
 * @file IGeofenceInterface.idl
 *
 * @brief 定义接口用于添加围栏，删除围栏，设置围栏回调函数等。
 *
 * 模块包路径：ohos.hdi.location.geofence.v1_0
 *
 * 引用：
 * - ohos.hdi.location.geofence.v1_0.IGeofenceCallback
 * - ohos.hdi.location.geofence.v1_0.GeofenceTypes
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.location.geofence.v1_0;

import ohos.hdi.location.geofence.v1_0.IGeofenceCallback;
import ohos.hdi.location.geofence.v1_0.GeofenceTypes;

/**
 * @brief 定义接口用于添加围栏，删除围栏，设置围栏回调函数等。
 *
 * @since 3.2
 */
interface IGeofenceInterface {
    /**
     * @brief 设置地理围栏回调函数。
     *
     * @param callback 表示地理围栏的回调函数，GNSS驱动使用此回调上报地理围栏服务可用性，
     * 上报地理围栏事件，上报地理围栏操作结果。详情参考{@link IGeofenceCallback}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    SetGeofenceCallback([in] IGeofenceCallback callbackObj);

    /**
     * @brief 添加一个地理围栏。
     *
     * @param fence 表示地理围栏的参数。详情参考{@link GeofenceInfo}。
     * @param monitorEvent 表示APP想要监控的地理围栏事件。详情参考{@link GeofenceEvent}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    AddGnssGeofence([in] struct GeofenceInfo fence,
                    [in] enum GeofenceEvent monitorEvent);

    /**
     * @brief 删除一个地理围栏。
     *
     * @param fenceIndex 表示地理围栏的编号。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    DeleteGnssGeofence([in] int fenceIndex);
}
/** @} */