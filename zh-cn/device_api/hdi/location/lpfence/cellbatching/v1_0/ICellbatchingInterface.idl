/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdiLpfenceCellbatching
 * @{
 *
 * @brief 为低功耗围栏服务提供基站轨迹数据记录的API。
 *
 * 本模块能够控制设备对接收的基站数据进行缓存和上报。
 *
 * 应用场景：根据设备接收的基站轨迹数据，判断用户的大致活动区域，进而进行一些服务提醒。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file ICellbatchingInterface.idl
 *
 * @brief 声明基站轨迹数据记录模块提供的API，用于使能和去使能基站轨迹数据记录功能，主动获取基站轨迹数据。
 *
 * 模块包路径：ohos.hdi.location.lpfence.cellbatching.v1_0
 *
 * 引用：
 * - ohos.hdi.location.lpfence.cellbatching.v1_0.CellbatchingTypes
 * - ohos.hdi.location.lpfence.cellbatching.v1_0.ICellbatchingCallback
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.location.lpfence.cellbatching.v1_0;

import ohos.hdi.location.lpfence.cellbatching.v1_0.CellbatchingTypes;
import ohos.hdi.location.lpfence.cellbatching.v1_0.ICellbatchingCallback;

/**
 * @brief 定义对基站轨迹数据记录模块进行基本操作的接口。
 *
 * 接口包含注册回调函数，取消注册回调函数，使能和去使能基站轨迹数据记录，主动获取基站轨迹数据。
 *
 * @since 4.0
 * @version 1.0
 */
interface ICellbatchingInterface {
    /**
     * @brief 注册回调函数。
     *
     * 用户在开启基站轨迹数据记录功能前，需要先注册该回调函数。当应用主动获取基站轨迹数据时，会通过回调函数进行上报。
     *
     * @param callbackObj 要注册的回调函数，只需成功订阅一次，无需重复订阅。详见{@link ICellbatchingCallback}。
     *
     * @return 如果注册回调函数成功，则返回0。
     * @return 如果注册回调函数失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    RegisterCellbatchingCallback([in] ICellbatchingCallback callbackObj);

    /**
     * @brief 取消注册回调函数。
     *
     * 取消之前注册的回调函数。当应用不需要使用基站轨迹数据记录功能，或需要更换回调函数时，需要取消注册回调函数。
     *
     * @param callbackObj 要取消注册的回调函数，只需成功取消订阅一次，无需重复取消订阅。详见{@link ICellbatchingCallback}。
     *
     * @return 如果取消注册回调函数成功，则返回0。
     * @return 如果取消注册回调函数失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    UnregisterCellbatchingCallback([in] ICellbatchingCallback callbackObj);

    /**
     * @brief 打开/关闭基站轨迹数据记录。
     *
     * 功能打开后，会将设备接收到的基站数据进行缓存，若接收到的基站数据与上一次接收的数据相同，则不会进行此次的数据缓存。
     *
     * @param req 设置基站轨迹数据记录参数。详见{@link CellbatchingRequest}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    CellbatchingSwitch([in] struct CellbatchingRequest req);

    /**
     * @brief 上报缓存的基站轨迹数据。
     *
     * 上报基站数据的个数与设备的缓存buffer大小、设备接收的基站数据个数有关。flush之后，底层缓存基站数据会被清空。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    FlushCellbatching();
}
/** @} */