/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiAudio
 * @{
 *
 * @brief Audio模块接口定义。
 *
 * 音频接口涉及数据类型、驱动加载接口、驱动适配器接口、音频播放接口、音频录音接口等。
 *
 * @since 4.1
 * @version 1.0
 */

/**
 * @file AudioTypes.idl
 *
 * @brief Audio模块接口定义中使用的数据类型。
 *
 * Audio模块接口定义中使用的数据类型，包括音频端口、适配器描述符、设备描述符、场景描述符、采样属性、时间戳等。
 *
 * @since 4.1
 * @version 1.0
 */
/**
 * @brief 音频接口的包路径。
 *
 * @since 4.1
 * @version 1.0
 */
package ohos.hdi.distributed_audio.audio.v1_0;

/**
 * @brief 音频端口的类型。
 *
 * @since 4.1
 * @version 1.0
 */
enum AudioPortDirection {
    PORT_OUT    = 1, /**< 音频输出端口。*/
    PORT_IN     = 2, /**< 音频输入端口。*/
    PORT_OUT_IN = 3, /**< 音频输出输入端口。*/
};

/**
 * @brief 音频端口上的Pin脚。
 *
 * @since 4.1
 * @version 1.0
 */
enum AudioPortPin {
    PIN_NONE                     = 0,                 /**< 无效端口 */
    PIN_OUT_SPEAKER              = 1 << 0,            /**< 喇叭输出 */
    PIN_OUT_HEADSET              = 1 << 1,            /**< 有线耳机输出 */
    PIN_OUT_LINEOUT              = 1 << 2,            /**< Lineout输出 */
    PIN_OUT_HDMI                 = 1 << 3,            /**< HDMI输出 */
    PIN_OUT_USB                  = 1 << 4,            /**< USB输出 */
    PIN_OUT_USB_EXT              = 1 << 5,            /**< USB外部声卡输出 */
    PIN_OUT_EARPIECE             = 1 << 5 | 1 << 4,   /**< 有线耳机输出 */
    PIN_OUT_BLUETOOTH_SCO        = 1 << 6,            /**< 蓝牙SCO输出 */
    PIN_OUT_DAUDIO_DEFAULT       = 1 << 7,            /**< 音频默认输出 */
    PIN_OUT_HEADPHONE            = 1 << 8,            /**< 有线耳机输出 */
    PIN_OUT_USB_HEADSET          = 1 << 9,            /**< ARM USB输出 */
    PIN_IN_MIC                   = 1 << 27 | 1 << 0,  /**< 麦克风输入 */
    PIN_IN_HS_MIC                = 1 << 27 | 1 << 1,  /**< 耳机麦克风输入 */
    PIN_IN_LINEIN                = 1 << 27 | 1 << 2,  /**< Linein输入 */
    PIN_IN_USB_EXT               = 1 << 27 | 1 << 3,  /**< USB外部声卡输入*/
    PIN_IN_BLUETOOTH_SCO_HEADSET = 1 << 27 | 1 << 4,  /**< 蓝牙SCO耳机输入 */
    PIN_IN_DAUDIO_DEFAULT        = 1 << 27 | 1 << 5,  /**< 音频默认输入 */
    PIN_IN_USB_HEADSET           = 1 << 27 | 1 << 6,  /**< ARM USB输入 */
};

/**
 * @brief 音频类型（场景）。
 *
 * @since 4.1
 * @version 1.0
 */
enum AudioCategory {
    AUDIO_IN_MEDIA         = 0, /**< 媒体 */
    AUDIO_IN_COMMUNICATION = 1, /**< 通信 */
    AUDIO_IN_RINGTONE      = 2, /**< 电话铃声 */
    AUDIO_IN_CALL          = 3, /**< 呼叫 */
    AUDIO_MMAP_NOIRQ       = 4, /**< Mmap模式 */
};

/**
 * @brief 音频格式。
 *
 * @since 4.1
 * @version 1.0
 */
enum AudioFormat {
    AUDIO_FORMAT_TYPE_PCM_8_BIT  = 1 << 0,                      /**< 8bit位宽PCM（Pulse Code Modulation）格式。*/
    AUDIO_FORMAT_TYPE_PCM_16_BIT = 1 << 1,                      /**< 16bit位宽PCM格式。*/
    AUDIO_FORMAT_TYPE_PCM_24_BIT = 1 << 1 | 1 << 0,             /**< 24bit位宽PCM格式。*/
    AUDIO_FORMAT_TYPE_PCM_32_BIT = 1 << 2,                      /**< 32bit位宽PCM格式。*/
    AUDIO_FORMAT_TYPE_AAC_MAIN   = 1 << 24 | 1 << 0,            /**< AAC main格式。*/
    AUDIO_FORMAT_TYPE_AAC_LC     = 1 << 24 | 1 << 1,            /**< AAC LC格式。*/
    AUDIO_FORMAT_TYPE_AAC_LD     = 1 << 24 | 1 << 1 | 1 << 0,   /**< AAC LD格式。*/
    AUDIO_FORMAT_TYPE_AAC_ELD    = 1 << 24 | 1 << 2,            /**< AAC ELD格式。*/
    AUDIO_FORMAT_TYPE_AAC_HE_V1  = 1 << 24 | 1 << 2 | 1 << 0,   /**< AAC HE_V1格式。*/
    AUDIO_FORMAT_TYPE_AAC_HE_V2  = 1 << 24 | 1 << 2 | 1 << 1,   /**< AAC HE_V2格式。*/
    AUDIO_FORMAT_TYPE_G711A      = 1 << 25 | 1 << 0,            /**< PCM G711A格式。*/
    AUDIO_FORMAT_TYPE_G711U      = 1 << 25 | 1 << 1,            /**< PCM G711u格式。*/
    AUDIO_FORMAT_TYPE_G726       = 1 << 25 | 1 << 1 | 1 << 0,   /**< PCM G726格式。*/
};

/**
 *@brief 音频通道掩码。
 *
 * 定义音频声道的位置掩码。
 *
 * @since 4.1
 * @version 1.0
 */
enum AudioChannelMask {
    AUDIO_CHANNEL_FRONT_LEFT  = 1,  /**< 声道布局前左。*/
    AUDIO_CHANNEL_FRONT_RIGHT = 2,  /**< 声道布局前右。*/
    AUDIO_CHANNEL_MONO        = 1,  /**< 单声道。*/
    AUDIO_CHANNEL_STEREO      = 3,  /**< 立体声，由左右声道组成。*/
};

/**
 * @brief 音频采样频率掩码。
 *
 * @since 4.1
 * @version 1.0
 */
enum AudioSampleRatesMask {
    AUDIO_SAMPLE_RATE_MASK_8000 = 1 << 0,          /**< 8K 采样频率。*/
    AUDIO_SAMPLE_RATE_MASK_12000 = 1 << 1,         /**< 12K 采样频率。*/
    AUDIO_SAMPLE_RATE_MASK_11025 = 1 << 2,         /**< 11.025K 采样频率。*/
    AUDIO_SAMPLE_RATE_MASK_16000 = 1 << 3,         /**< 16K 采样频率。*/
    AUDIO_SAMPLE_RATE_MASK_22050 = 1 << 4,         /**< 22.050K 采样频率。*/
    AUDIO_SAMPLE_RATE_MASK_24000 = 1 << 5,         /**< 24K 采样频率。*/
    AUDIO_SAMPLE_RATE_MASK_32000 = 1 << 6,         /**< 32K 采样频率。*/
    AUDIO_SAMPLE_RATE_MASK_44100 = 1 << 7,         /**< 44.1K 采样频率。*/
    AUDIO_SAMPLE_RATE_MASK_48000 = 1 << 8,         /**< 48K 采样频率。*/
    AUDIO_SAMPLE_RATE_MASK_64000 = 1 << 9,         /**< 64K 采样频率。*/
    AUDIO_SAMPLE_RATE_MASK_96000 = 1 << 10,        /**< 96K 采样频率。*/
    AUDIO_SAMPLE_RATE_MASK_INVALID = 4294967295,   /**< 无效的采样频率。*/
};

/**
 * @brief 音频端口的数据透传模式。
 *
 * @since 4.1
 * @version 1.0
 */
enum AudioPortPassthroughMode {
    PORT_PASSTHROUGH_LPCM    = 1 << 0, /**< 立体声PCM。*/
    PORT_PASSTHROUGH_RAW     = 1 << 1, /**< HDMI透传。*/
    PORT_PASSTHROUGH_HBR2LBR = 1 << 2, /**< 蓝光次世代音频降规格输出。*/
    PORT_PASSTHROUGH_AUTO    = 1 << 3, /**< 根据HDMI EDID能力自动匹配。*/
};

/**
 * @brief 原始音频样本格式。
 *
 * @since 4.1
 * @version 1.0
 */
enum AudioSampleFormat {
    /* 8 bits */
    AUDIO_SAMPLE_FORMAT_S8 = 0,  /**< 8bit位宽有符号交织样本。**/
    AUDIO_SAMPLE_FORMAT_S8P = 1, /**< 8bit位宽有符号非交织样本。**/
    AUDIO_SAMPLE_FORMAT_U8 = 2,  /**< 8bit位宽无符号交织样本。**/
    AUDIO_SAMPLE_FORMAT_U8P = 3, /**< 8bit位宽无符号非交织样本。**/
    /* 16 bits */
    AUDIO_SAMPLE_FORMAT_S16 = 4,  /**< 16bit位宽有符号交织样本。**/
    AUDIO_SAMPLE_FORMAT_S16P = 5, /**< 16bit位宽有符号非交织样本。**/
    AUDIO_SAMPLE_FORMAT_U16 = 6,  /**< 16bit位宽无符号符号交织样本。**/
    AUDIO_SAMPLE_FORMAT_U16P = 7, /**< 16bit位宽无符号符号非交织样本。**/
    /* 24 bits */
    AUDIO_SAMPLE_FORMAT_S24 = 8,   /**< 24bit位宽有符号交织样本。**/
    AUDIO_SAMPLE_FORMAT_S24P = 9,  /**< 24bit位宽有符号非交织样本。**/
    AUDIO_SAMPLE_FORMAT_U24 = 10,  /**< 24bit位宽无符号符号交织样本。**/
    AUDIO_SAMPLE_FORMAT_U24P = 11, /**< 24bit位宽无符号非交织样本。**/
    /* 32 bits */
    AUDIO_SAMPLE_FORMAT_S32 = 12,  /**< 32bit位宽有符号交织样本。**/
    AUDIO_SAMPLE_FORMAT_S32P = 13, /**< 32bit位宽有符号非交织样本。**/
    AUDIO_SAMPLE_FORMAT_U32 = 14,  /**< 32bit位宽无符号交织样本。**/
    AUDIO_SAMPLE_FORMAT_U32P = 15, /**< 32bit位宽无符号非交织样本。**/
    /* 64 bits */
    AUDIO_SAMPLE_FORMAT_S64 = 16,  /**< 64bit位宽有符号交织样本。**/
    AUDIO_SAMPLE_FORMAT_S64P = 17, /**< 64bit位宽有符号非交织样本。**/
    AUDIO_SAMPLE_FORMAT_U64 = 18,  /**< 64bit位宽无符号交织样本。**/
    AUDIO_SAMPLE_FORMAT_U64P = 19, /**< 64bit位宽无符号非交织样本。**/
    /* float double */
    AUDIO_SAMPLE_FORMAT_F32 = 20,  /**< 32bit位宽浮点型交织样本。**/
    AUDIO_SAMPLE_FORMAT_F32P = 21, /**< 32bit位宽浮点型非交织样本。**/
    AUDIO_SAMPLE_FORMAT_F64 = 22,  /**< 64bit位宽双精度浮点型交织样本。**/
    AUDIO_SAMPLE_FORMAT_F64P = 23, /**< 64bit位宽双精度浮点型非交织样本。**/
};

/**
 * @brief 音频播放的通道模式。
 *
 * @attention 下面的模式是针对双通道立体声的音频播放而设置，其他不支持。
 *
 * @since 4.1
 * @version 1.0
 */
enum AudioChannelMode {
    AUDIO_CHANNEL_NORMAL     = 0, /**< 正常模式，不做处理。*/
    AUDIO_CHANNEL_BOTH_LEFT  = 1, /**< 两个声道全部为左声道声音。*/
    AUDIO_CHANNEL_BOTH_RIGHT = 2, /**< 两个声道全部为右声道声音。*/
    AUDIO_CHANNEL_EXCHANGE   = 3, /**< 左右声道数据互换，左声道为右声道声音，右声道为左声道声音。*/
    AUDIO_CHANNEL_MIX        = 4, /**< 左右两个声道输出为左右声道相加（混音）。*/
    AUDIO_CHANNEL_LEFT_MUTE  = 5, /**< 左声道静音，右声道播放原右声道声音。*/
    AUDIO_CHANNEL_RIGHT_MUTE = 6, /**< 右声道静音，左声道播放原左声道声音。*/
    AUDIO_CHANNEL_BOTH_MUTE  = 7, /**< 左右声道均静音。*/
};

/**
 * @brief 音频数据结束类型。
 *
 * @since 4.1
 * @version 1.0
 */
enum AudioDrainNotifyType {
    AUDIO_DRAIN_NORMAL_MODE = 0, /**< 曲目的所有数据播放完就结束。*/
    AUDIO_DRAIN_EARLY_MODE  = 1, /**<曲目的所有数据未播放完就结束，以便给音频服务做连续性曲目切换留出时间。*/

};

/**
 * @brief 回调函数通知事件类型。
 *
 * @since 4.1
 * @version 1.0
 */
enum AudioCallbackType {
    AUDIO_NONBLOCK_WRITE_COMPLETED = 0, /**< 非阻塞式写完成。*/
    AUDIO_DRAIN_COMPLETED          = 1, /**< DrainBuffer完成，详情参考{@link DrainBuffer}。*/
    AUDIO_FLUSH_COMPLETED          = 2, /**< Flush完成，详情参考{@link Flush}。*/
    AUDIO_RENDER_FULL              = 3, /**< 录音缓冲区已满。*/
    AUDIO_ERROR_OCCUR              = 4, /**< 发生了错误。*/
};

/**
 * @brief 音频端口角色。
 *
 * @since 4.1
 * @version 1.0
 */
enum AudioPortRole {
    AUDIO_PORT_UNASSIGNED_ROLE = 0, /**< 未指定端口角色。*/
    AUDIO_PORT_SOURCE_ROLE     = 1, /**< 指定端口为发送端角色。*/
    AUDIO_PORT_SINK_ROLE       = 2, /**< 指定端口为接受端角色。*/
};

/**
 * @brief 音频端口类型。
 *
 * @since 4.1
 * @version 1.0
 */
enum AudioPortType {
    AUDIO_PORT_UNASSIGNED_TYPE = 0, /**< 未指定端口类型。*/
    AUDIO_PORT_DEVICE_TYPE     = 1, /**< 指定端口为设备类型。*/
    AUDIO_PORT_MIX_TYPE        = 2, /**< 指定端口为复合类型。*/
    AUDIO_PORT_SESSION_TYPE    = 3, /**< 指定端口为会话类型。*/
};

/**
 * @brief 端口会话类型。
 *
 * @since 4.1
 * @version 1.0
 */
enum AudioSessionType {
    AUDIO_OUTPUT_STAGE_SESSION = 0, /**< 会话绑定到指定输出流。*/
    AUDIO_OUTPUT_MIX_SESSION   = 1, /**< 会话绑定到特定音轨。*/
    AUDIO_ALLOCATE_SESSION     = 2, /**< 会话ID需重新申请。*/
    AUDIO_INVALID_SESSION      = 3, /**< 无效会话类型。*/
};

/**
 * @brief 音频设备类型。
 *
 * @since 4.1
 * @version 1.0
 */
enum AudioDeviceType {
    AUDIO_LINEOUT        = 1 << 0, /**< LINEOUT设备。*/
    AUDIO_HEADPHONE      = 1 << 1, /**< 耳机。*/
    AUDIO_HEADSET        = 1 << 2, /**< 头戴式耳机。*/
    AUDIO_USB_HEADSET    = 1 << 3, /**< USB头戴式耳机。*/
    AUDIO_USB_HEADPHONE  = 1 << 4, /**< USB耳机。*/
    AUDIO_USBA_HEADSET   = 1 << 5, /**< USB模拟头戴式耳机。*/
    AUDIO_USBA_HEADPHONE = 1 << 6, /**< USB模拟耳机。*/
    AUDIO_PRIMARY_DEVICE = 1 << 7, /**< 主音频设备。*/
    AUDIO_USB_DEVICE     = 1 << 8, /**< USB音频设备。*/
    AUDIO_A2DP_DEVICE    = 1 << 9, /**< 蓝牙音频设备。*/
    AUDIO_HDMI_DEVICE    = 1 << 10, /**< HDMI音频设备 */
    AUDIO_ADAPTER_DEVICE = 1 << 11, /**< 声卡设备 */
    AUDIO_DEVICE_UNKNOWN,          /**< 未知设备。*/
};

/**
 * @brief 音频事件类型。
 *
 * @since 4.1
 * @version 1.0
 */
enum AudioEventType {
    AUDIO_DEVICE_ADD        = 1,  /**< 音频设备添加。*/
    AUDIO_DEVICE_REMOVE     = 2,  /**< 音频设备移除。*/
    AUDIO_LOAD_SUCCESS      = 3,  /**< 声卡加载成功。*/
    AUDIO_LOAD_FAILURE      = 4,  /**< 声卡加载失败。*/
    AUDIO_UNLOAD            = 5,  /**< 声卡卸载。*/
    AUDIO_SERVICE_VALID     = 7,  /**< 音频服务可用。*/
    AUDIO_SERVICE_INVALID   = 8,  /**< 音频服务不可用。*/
    AUDIO_CAPTURE_THRESHOLD = 9,  /**< 录音阈值上报。*/
    AUDIO_EVENT_UNKNOWN     = 10, /**< 未知事件。*/
};

/**
 * @brief 音频扩展参数键类型。
 *
 * @since 4.1
 * @version 1.0
 */
enum AudioExtParamKey {
    AUDIO_EXT_PARAM_KEY_NONE     = 0,    /**< 分布式音频-无效事件。*/
    AUDIO_EXT_PARAM_KEY_VOLUME   = 1,    /**< 分布式音频-音量事件。*/
    AUDIO_EXT_PARAM_KEY_FOCUS    = 2,    /**< 分布式音频-焦点事件。*/
    AUDIO_EXT_PARAM_KEY_BUTTON   = 3,    /**< 分布式音频-媒体按钮事件。*/
    AUDIO_EXT_PARAM_KEY_EFFECT   = 4,    /**< 分布式音频-音频效果事件。*/
    AUDIO_EXT_PARAM_KEY_STATUS   = 5,    /**< 分布式音频-设备状态事件。*/
    AUDIO_EXT_PARAM_KEY_USB_DEVICE = 101, /**< USB设备类型（ ARM 或 HIFI）*/
    AUDIO_EXT_PARAM_KEY_LOWPOWER = 1000, /**< 低电量事件。*/
};

/**
 * @brief 音频设备状态。
 *
 * @since 4.1
 * @version 1.0
 */
struct AudioDeviceStatus {
    unsigned int pnpStatus; /**< 音频PnP设备状态。*/
};

/**
 * @brief 音频场景描述。
 *
 * @since 4.1
 * @version 1.0
 */
union SceneDesc {
    unsigned int id; /**< 音频场景的ID。*/
};

/**
 * @brief 音频端口。
 *
 * @since 4.1
 * @version 1.0
 */
struct AudioPort {
    enum AudioPortDirection dir; /**< 音频端口的类型，详情参考{@link AudioPortDirection}。*/
    unsigned int portId;         /**< 音频端口的ID。*/
    String portName;             /**< 音频端口的名称。*/
};

/**
 * @brief 音频适配器描述符。
 *
 * 一个音频适配器（adapter）是一个声卡的端口驱动集合，包含输出端口、输入端口，
 * 其中一个端口对应着多个PIN脚，一个Pin脚对应着一个实体的器件（例如喇叭、有线耳机）。
 *
 * @since 4.1
 * @version 1.0
 */
struct AudioAdapterDescriptor {
    String adapterName;       /**< 音频适配器的名称。*/
    struct AudioPort[] ports; /**< 一个音频适配器支持的端口列表。*/
};

/**
 * @brief 音频设备描述符。
 *
 * @since 4.1
 * @version 1.0
 */
struct AudioDeviceDescriptor {
    unsigned int portId;    /**< 音频端口ID。*/
    enum AudioPortPin pins; /**< 音频端口上的Pin脚（输出、输入），详情参考{@link AudioPortPin}。*/
    String desc;            /**< 以字符串命名的音频设备。*/
};

/**
 * @brief 音频场景描述符。
 *
 * @since 4.1
 * @version 1.0
 */
struct AudioSceneDescriptor {
    union SceneDesc scene;             /**< 音频场景描述。*/
    struct AudioDeviceDescriptor desc; /**< 音频设备描述符。*/
};

/**
 * @brief 音频输入类型。
 *
 * @since 4.1
 * @version 1.0
 */
enum AudioInputType {
    AUDIO_INPUT_DEFAULT_TYPE             = 0,      /**< 默认输入 */
    AUDIO_INPUT_MIC_TYPE                 = 1 << 0, /**< 麦克风输入 */
    AUDIO_INPUT_SPEECH_WAKEUP_TYPE       = 1 << 1, /**< 语音唤醒输入 */
    AUDIO_INPUT_VOICE_COMMUNICATION_TYPE = 1 << 2, /**< 通话 */
    AUDIO_INPUT_VOICE_RECOGNITION_TYPE   = 1 << 3, /**< 声音识别 */
};

/**
 * @brief 音频采样属性。
 *
 * @since 4.1
 * @version 1.0
 */
struct AudioSampleAttributes {
    enum AudioCategory type;       /**< 音频类型，详情参考 {@link AudioCategory} */
    boolean interleaved;           /**< 音频数据交织的标记。*/
    enum AudioFormat format;       /**< 音频数据格式，详情参考 {@link AudioFormat}. */
    unsigned int sampleRate;       /**< 音频采样频率。*/
    unsigned int channelCount;     /**< 音频通道数目，如单通道为1、立体声为2。*/
    unsigned int period;           /**< 音频采样周期，单位赫兹。*/
    unsigned int frameSize;        /**< 音频数据的帧大小。*/
    boolean isBigEndian;           /**< 音频数据的大端标志。*/
    boolean isSignedData;          /**< 音频数据有符号或无符号标志。*/
    unsigned int startThreshold;   /**< 音频播放起始阈值。*/
    unsigned int stopThreshold;    /**< 音频播放停止阈值。*/
    unsigned int silenceThreshold; /**< 录音缓冲区阈值。*/
    int streamId;                  /**< 录音或播放的标识符。*/
    int sourceType;                /**< 播放或录音的音源类型。*/
};

/**
 * @brief 音频时间戳。
 *
 * @since 4.1
 * @version 1.0
 */
struct AudioTimeStamp {
    long tvSec;  /**< tvSec时间，单位：秒。 */
    long tvNSec; /**< tvNSec时间，单位：纳秒。 */
};

/**
 * @brief 音频子端口的支持能力。
 *
 * @since 4.1
 * @version 1.0
 */
struct AudioSubPortCapability {
    unsigned int portId;                /**< 子端口ID。*/
    String desc;                        /**< 以字符串命名的子端口。*/
    enum AudioPortPassthroughMode mask; /**< 数据透传模式，详情参考 {@link AudioPortPassthroughMode}。*/
};

/**
 * @brief 音频端口的支持能力。
 *
* @since 4.1
 * @version 1.0
 */
struct AudioPortCapability {
    unsigned int deviceType;                       /**< 设备输出、输入类型。*/
    unsigned int deviceId;                         /**< 设备ID，唯一的设备识别符。*/
    boolean hardwareMode;                          /**< 是否支持设备绑定处理。*/
    unsigned int formatNum;                        /**< 支持的音频格式数目。*/
    enum AudioFormat[] formats;                    /**< 支持的音频格式，详情参考{@link AudioFormat}。*/
    unsigned int sampleRateMasks;                  /**< 支持的音频采样频率（8k、16k、32k、48k）。*/
    enum AudioChannelMask channelMasks;            /**< 设备的声道布局掩码，详情参考{@link AudioChannelMask}。*/
    unsigned int channelCount;                     /**< 最大支持的声道总数。*/
    struct AudioSubPortCapability[] subPorts;      /**< 支持的子端口列表。*/
    enum AudioSampleFormat[] supportSampleFormats; /**< 支持的采样率列表，详情参考{@link AudioSampleFormat}。*/
};

/**
 * @brief mmap缓冲区描述符。
 *
 * @since 4.1
 * @version 1.0
 */
struct AudioMmapBufferDescriptor {
    byte[] memoryAddress;  /**< 指向mmap缓冲区的指针。*/
    FileDescriptor memoryFd;          /**< mmap缓冲区的文件描述符。*/
    int totalBufferFrames; /**< 缓冲区总大小，单位：帧。*/
    int transferFrameSize; /**< 传输大小，单位：帧。*/
    int isShareable;       /**< mmap缓冲区是否可以在进程间共享。*/
    unsigned int offset;   /**< 文件偏移。*/
    String filePath;       /**< mmap文件路径。*/
};

/**
 * @brief 音频设备拓展信息。
 *
 * @since 4.1
 * @version 1.0
 */
struct AudioDevExtInfo {
    int moduleId;  /**< 音频流绑定的模块ID。*/
    enum AudioPortPin type; /**< 音频端口上的Pin脚（输出、输入），详情参考{@link AudioPortPin}。*/
    String desc;            /**< 地址描述。*/
};

/**
 * @brief 音轨拓展信息。
 *
 * @since 4.1
 * @version 1.0
 */
struct AudioMixExtInfo {
    int moduleId; /**< 流所属模块标识符。*/
    int streamId; /**< 由调用者传递的Render或Capture标识符。*/
};

/**
 * @brief 会话拓展信息。
 *
 * @since 4.1
 * @version 1.0
 */
struct AudioSessionExtInfo {
    enum AudioSessionType sessionType; /**< 音频会话类型。*/
};

/**
 * @brief 音频端口特定信息。
 *
 * @since 4.1
 * @version 1.0
 */
struct AudioInfo {
    struct AudioDevExtInfo device;      /**< 设备特定信息，详情参考{@link AudioDevExtInfo}。*/
    struct AudioMixExtInfo mix;         /**< 音轨特定信息，详情参考{@link AudioMixExtInfo}。*/
    struct AudioSessionExtInfo session; /**< 会话特定信息，详情参考{@link AudioSessionExtInfo}。*/
};

/**
 * @brief 音频路由节点。
 *
 * @since 4.1
 * @version 1.0
 */
struct AudioRouteNode {
    int portId;     /**< 音频端口ID。*/
    enum AudioPortRole role; /**< 指定端口角色为发送端或接收端，详情参考{@link AudioPortRole}。*/
    enum AudioPortType type; /**< 指定端口类型可以为设备类型、复合类型、绘画类型，详情参考{@link AudioPortType}。*/
    struct AudioInfo ext;    /**< 音频端口特定信息，详情参考{@link AudioInfo}。*/
};

/**
 * @brief 音频路由信息。
 *
 * @since 4.1
 * @version 1.0
 */
struct AudioRoute {
    struct AudioRouteNode[] sources; /**< 发送端列表。*/
    struct AudioRouteNode[] sinks;   /**< 接受端列表。*/
};

/**
 * @brief 音频事件。
 *
 * @since 4.1
 * @version 1.0
 */
struct AudioEvent {
    unsigned int eventType;  /**< 事件类型，详情参考{@link enum AudioEventType}。*/
    unsigned int deviceType; /**< 设备类型，详情参考{@link enum AudioDeviceType}。*/
};
/** @} */