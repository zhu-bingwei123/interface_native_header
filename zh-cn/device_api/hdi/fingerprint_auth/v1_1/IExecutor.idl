/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfFingerprintAuth
 * @{
 *
 * @brief 提供指纹认证驱动的API接口。
 *
 * 指纹认证驱动程序为指纹认证服务提供统一的接口，用于访问指纹认证驱动程序。获取指纹认证驱动代理后，服务可以调用相关API获取执行器。
 * 获取指纹认证执行器后，服务可以调用相关API获取执行器信息，获取凭据模板信息、注册指纹特征模板、进行用户指纹认证、删除指纹特征模板等。
 *
 * @since 4.0
 */

/**
 * @file IExecutor.idl
 *
 * @brief 定义执行器接口，用于获取执行器，获取凭据模版信息，注册指纹特征模版，进行用户指纹认证，删除指纹特征模版等。
 *
 * 模块包路径：ohos.hdi.fingerprint_auth.v1_1
 *
 * 引用：
 * - ohos.hdi.fingerprint_auth.v1_0.FingerprintAuthTypes
 * - ohos.hdi.fingerprint_auth.v1_0.IExecutor
 * - ohos.hdi.fingerprint_auth.v1_0.IExecutorCallback
 * - ohos.hdi.fingerprint_auth.v1_1.FingerprintAuthTypes
 * - ohos.hdi.fingerprint_auth.v1_1.ISaCommandCallback
 *
 * @since 4.0
 */

package ohos.hdi.fingerprint_auth.v1_1;

import ohos.hdi.fingerprint_auth.v1_0.FingerprintAuthTypes;
import ohos.hdi.fingerprint_auth.v1_0.IExecutor;
import ohos.hdi.fingerprint_auth.v1_0.IExecutorCallback;
import ohos.hdi.fingerprint_auth.v1_1.FingerprintAuthTypes;
import ohos.hdi.fingerprint_auth.v1_1.ISaCommandCallback;

/**
 * @brief 定义执行器接口，用于获取执行器，获取凭据模版信息，注册指纹特征模版，进行用户指纹认证，删除指纹特征模版等。
 *
 * @since 4.0
 * @version 1.1
 */
interface IExecutor extends ohos.hdi.fingerprint_auth.v1_0.IExecutor {
    /**
     * @brief 指纹识别。
     *
     * @param scheduleId 调度ID，用于标识一次操作请求的调度过程。
     * @param templateIdList 指定要认证的模版ID列表。
     * @param endAfterFirstFail 第一次认证失败后结束认证。
     * @param extraInfo 其他相关信息，用于支持信息扩展。
     * @param callbackObj 回调对象{@link IExecutorCallback}。
     * 
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 4.0
     * @version 1.1
     */
    AuthenticateV1_1([in] unsigned long scheduleId, [in] unsigned long[] templateIdList, [in] boolean endAfterFirstFail, [in] unsigned char[] extraInfo, [in] IExecutorCallback callbackObj);
    /**
     * @brief 获取指纹执行器属性。
     *
     * @param templateIdList 指定要认证的模版ID列表。
     * @param propertyTypes 指纹执行器属性类型，见{@link GetPropertyType}。
     * @param property 指纹执行器属性{@link Property}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 4.0
     * @version 1.1
     */
    GetProperty([in] unsigned long[] templateIdList, [in] enum GetPropertyType[] propertyTypes, [out] struct Property property);
    /**
     * @brief 设置指纹缓存模板。
     *
     * @param templateIdList 指纹缓存模板列表。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 4.0
     * @version 1.1
     */
    SetCachedTemplates([in] unsigned long[] templateIdList);
    /**
     * @brief 注册sa命令回调。
     *
     * @param callbackObj sa命令回调对象。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     */
    RegisterSaCommandCallback([in] ISaCommandCallback callbackObj);
}
/** @} */