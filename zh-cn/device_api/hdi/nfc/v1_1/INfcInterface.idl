/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiNfc
 * @{
 *
 * @brief 为nfc服务提供统一的访问nfc驱动的接口。
 *
 * NFC服务通过获取的nfc驱动对象提供的API接口访问nfc驱动，包括开关NFC、初始化NFC、读写数据、配置RF参数、
 * 通过IO控制发送NCI指令给nfc驱动
 *
 * @since 4.1
 * @version 1.1
 */

/**
 * @file INfcInterface.idl
 *
 * @brief 定义NFC扩展的查询配置、工厂级复位的适配接口文件。
 *
 * 模块包路径：ohos.hdi.nfc.v1_1
 *
 * 引用：
 * - ohos.hdi.nfc.v1_0.NfcTypes
 * - ohos.hdi.nfc.v1_0.INfcInterface
 * - ohos.hdi.nfc.v1_1.NfcTypes
 *
 * @since 4.1
 * @version 1.1
 */

package ohos.hdi.nfc.v1_1;

import ohos.hdi.nfc.v1_0.NfcTypes;
import ohos.hdi.nfc.v1_0.INfcInterface;
import ohos.hdi.nfc.v1_1.NfcTypes;

/**
 * @brief 声明操作nfc芯片的API，包括关闭、打开nfc，初始化nfc，读写数据、配置RF参数、发送nci指令。
 *
 * @since 4.1
 * @version 1.1
 */
interface INfcInterface extends ohos.hdi.nfc.v1_0.INfcInterface {
    /**
     * @brief 查询厂商自定义的NFC配置。
     * 
     * @param config 厂商自定义的NFC配置。
     * @return 操作成功返回0，否则返回失败。
     * 具体类型详见{@link NfcTypes}。
     *
     * @since 4.1
     * @version 1.0
     */
    GetVendorConfig([out] struct NfcVendorConfig config, [out] enum NfcStatus status);

    /**
     * @brief NFC芯片工厂级复位。
     * 
     * @return 操作成功返回0，否则返回失败。
     * 具体类型详见{@link NfcTypes}。
     *
     * @since 4.1
     * @version 1.0
     */
     DoFactoryReset([out] enum NfcStatus status);

    /**
     * @brief 关闭NFC。如果设备支持关机刷卡功能，需要实现该接口
     * 
     * @return 操作成功返回0，否则返回失败。
     * 具体类型详见{@link NfcTypes}。
     *
     * @since 4.1
     * @version 1.0
     */
     Shutdown([out] enum NfcStatus status);
}
/** @} */