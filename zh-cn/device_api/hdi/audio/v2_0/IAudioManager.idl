/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiAudio
 * @{
 *
 * @brief Audio模块接口定义。
 *
 * 音频接口涉及数据类型、驱动加载接口、驱动适配器接口、音频播放接口、音频录音接口等。
 *
 * @since 4.1
 * @version 2.0
 */

/**
 * @file IAudioManager.idl
 *
 * @brief Audio适配器管理及加载的接口定义文件。
 *
 * 模块包路径：ohos.hdi.audio.v2_0
 *
 * 引用：
 * - ohos.hdi.audio.v2_0.AudioTypes
 * - ohos.hdi.audio.v2_0.IAudioAdapter
 *
 * @since 4.1
 * @version 2.0
 */


package ohos.hdi.audio.v2_0;

import ohos.hdi.audio.v2_0.AudioTypes;
import ohos.hdi.audio.v2_0.IAudioAdapter;

/**
 * @brief AudioManager音频适配器管理接口。
 *
 * 按照音频服务下发的音频适配器（声卡）描述符加载一个具体的音频适配器驱动程序。
 *
 * @see IAudioAdapter
 *
 * @since 4.1
 * @version 2.0
 */
interface IAudioManager {

    /**
     * @brief 获取音频驱动中支持的所有适配器的列表。
     *
     * @param descs 获取到的音频适配器列表保存到descs中，详请参考{@link AudioAdapterDescriptor}。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see LoadAdapter
     *
     * @since 4.1
     * @version 2.0
     */
    GetAllAdapters([out] struct AudioAdapterDescriptor[] descs);

    /**
     * @brief 加载一个音频适配器（声卡）的驱动。
     *
     * 加载一个具体的音频驱动，例如usb驱动，在具体实现中可能加载的是一个动态链接库（*.so）。
     *
     * @param desc 待加载的音频适配器描述符，详请参考{@link AudioAdapterDescriptor}。
     * @param adapter 获取的音频适配器接口的对象实例保存到adapter中，详请参考{@link IAudioAdapter}。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see GetAllAdapters
     * @see UnloadAdapter
     *
     * @since 4.1
     * @version 2.0
     */
    LoadAdapter([in] struct AudioAdapterDescriptor desc, [out] IAudioAdapter adapter);

    /**
     * @brief 卸载音频适配器（声卡）的驱动。
     *
     * @param adapterName 待卸载的音频适配器接口的对象名称。
     *
     * @see LoadAdapter
     *
     * @since 4.1
     * @version 2.0
     */
    UnloadAdapter([in] String adapterName);

    /**
     * @brief 释放音频管理接口对象。
     *
     * @return 功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 2.0
     */
    ReleaseAudioManagerObject();
}
/** @} */
